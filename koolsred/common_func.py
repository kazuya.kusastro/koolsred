#!/usr/bin/python3
# coding: utf-8

### common_func.py: define common functions

### import libraries

### define functions
## file names
def add_suffix(input_list, suffix):
    output_list = []
    for input_name in input_list:
        if input_name.endswith('.fits'):
            output_name = input_name[:-len('.fits')] + suffix + '.fits'
        else:
            output_name = input_fits + suffix + '.fits'

        output_list.append(output_name)

    return output_list

def convert_list_to_str(input_list):
    input_list_str = ''
    for tmp_str in input_list:
        input_list_str += tmp_str + ','

    return input_list_str.rstrip(',')

def divide_frame_list(input_frame_list, divide_num):
# get quotient and remainder
    quot, rem = divmod(len(input_frame_list), divide_num)

    len_list = [quot] * divide_num
    for count in range(rem):
        len_list[count] += 1

    output_frame_list = []
    len_sum = 0
    for tmp_len in len_list:
        output_frame_list.append(input_frame_list[len_sum:(len_sum + tmp_len)])
        len_sum += tmp_len

    return output_frame_list
