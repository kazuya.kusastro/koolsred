#!/usr/bin/python3
# coding: utf-8

### main_window.py: define the main window of KOOLS-IFU data reduction GUI

### import libraries
#import argparse
import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow, QTabWidget, QMessageBox, QGridLayout)
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

# KOOLS reduction modules
from koolsred import const_var
from koolsred import gui

## define main window class
class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.frame_setting = gui.setting.boxSetting(self)
        self.frameInput = gui.input_frame.boxInput(self)
#    self.frameOverscanBias = koolsred.GUI.OverscanBias.boxOverscanBias(self)
#    self.frameCombFlatComp = koolsred.GUI.CombFlatComp.boxCombineFlat(self)
        self.frameBiasFlat = gui.bias_flat.boxBiasFlat(self)
#    self.frameNoGrismImage = koolsred.GUI.NoGrismImage.boxNoGrismImage(self)
#    self.frameQuickImage = koolsred.GUI.QuickImage.boxQuickImage(self)
        self.frameExtractSpec = gui.extract_spec.boxExtractSpec(self)
        self.frameWaveCalib = gui.wave_calib.boxWaveCalib(self)
        self.frame_sky_subtract = gui.sky_subtract.Frame(self)

        tabs = QTabWidget()
        tabs.addTab(self.frame_setting, 'Settings')
        tabs.addTab(self.frameInput, 'Input files')
#    tabs.addTab(self.frameOverscanBias, 'Overscan and bias')
        tabs.addTab(self.frameBiasFlat, 'Bias and flat')
#    tabs.addTab(self.frameNoGrismImage, 'Make image (without grism)')
#    tabs.addTab(self.frameQuickImage, 'Make quick image (with grism)')
        tabs.addTab(self.frameExtractSpec, 'Extract spectra')
        tabs.addTab(self.frameWaveCalib, 'Wavelength calibration')
        tabs.addTab(self.frame_sky_subtract, 'Sky subtraction')
        tabs.setFont(const_var.font)
        tabs.currentChanged.connect(self.updateGUI)

        self.frameOpenFits = gui.open_fits.boxOpenFits(self)

# layout
#    self.layout = QVBoxLayout()
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(tabs, 0, 0)
        layout.addWidget(self.frameOpenFits, 1, 0)

        self.setLayout(layout)
        self.resize(800, 600)
        self.setWindowTitle('KOOLS-IFU data reduction GUI')

# function
    def updateGUI(self):
        self.frame_setting.update_gui()
        self.frameInput.updateGUI()
#    self.frameNoGrismImage.updateGUI()
#    self.frameQuickImage.updateGUI()
        self.frameExtractSpec.updateGUI()
        self.frameWaveCalib.updateGUI()
        self.frame_sky_subtract.update_gui()

#  def closeEvent(self, event):
#    reply = QMessageBox.question(self, 'Message', 'Are you sure to close this GUI?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
##    reply.setFont(const_var.fontLabel)
#
#    if reply == QMessageBox.Yes:
#      event.accept()
#    else:
#      event.ignore()

### main function
def show(args):
# argparse
#  parser = argparse.ArgumentParser(description='KOOLS-IFU data reduction GUI')
#  parser.add_argument('-o', '--observation', action='store_true', help='Observation mode.')
#
#  args = parser.parse_args()

# observation mode
#  if args.observation:
#    koolsred.CommonFunc.copyObsFile()

# start GUI
    app = QApplication(sys.argv)
    main_window = MainWindow()

# show main window
    main_window.show()
# exit main function if exit called
    sys.exit(app.exec_())

    return
