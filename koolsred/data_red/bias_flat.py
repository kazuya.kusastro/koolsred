#!/usr/bin/python3
# coding: utf-8

### data_red/bias_flat.py: define data reduction functions for overscan, bias, and flat

### import libraries
import argparse
import math
import os
import sys

from astropy.io import fits
import astroscrappy
import numpy as np

def subtract_cut_overscan(input_fits, output_fits):
# set constants
    mean_x_pix = ((522, 536), (536, 550), (1594, 1608), (1608, 1622))
    mean_y_pix = ((20, 120), (1520, 1620))
    cut_x_pix = ((9, 521), (551, 1063), (1081, 1593), (1623, 2135))
    ch_x_pix = 512

# input fits file
    data_in, header = fits.getdata(input_fits, view = np.ndarray, header = True)
    data_out = np.zeros((1640, 2048))

    for count_ch, (tmp_mean_x, tmp_cut_x) in enumerate(zip(mean_x_pix, cut_x_pix)):
# calculate mean counts at overscan regions
        tmp_data1 = data_in[mean_y_pix[0][0]:mean_y_pix[0][1], tmp_mean_x[0]:tmp_mean_x[1]]
        tmp_data2 = data_in[mean_y_pix[1][0]:mean_y_pix[1][1], tmp_mean_x[0]:tmp_mean_x[1]]

#    tmp_mean = np.concatenate((tmp_data1, tmp_data2)).reshape((-1)).mean()
        tmp_mean = (tmp_data1.sum() + tmp_data2.sum()) / (tmp_data1.size + tmp_data2.size)
#    print(tmp_mean, type(tmp_mean))

# cut and subtract overscan regions
        data_out[:, (count_ch * ch_x_pix):((count_ch + 1) * ch_x_pix)] = data_in[:, tmp_cut_x[0]:tmp_cut_x[1]] - tmp_mean
#    data_out.extend(tmp_data.T.tolist())

    fits.writeto(output_fits, data_out, header, overwrite = True)
    print('{} --> {}'.format(input_fits, output_fits))

    return

def combine_frames(input_list, output_fits, combine_mode):
# initialize
    data_in = []
    input_fits_list = input_list.split(',')

# input fits files
    for tmp_input in input_fits_list:
        data_in_tmp, header = fits.getdata(tmp_input, header=True)
        data_in.append(data_in_tmp)

    data_in_np = np.array(data_in)

# median
    if combine_mode == 'median':
        data_out = np.median(data_in_np, axis=0)

    elif combine_mode == 'mean':
        data_out = np.mean(data_in_np, axis=0)

    fits.writeto(output_fits, data_out, header, overwrite=True)
    print('{} --> {}'.format(input_list, output_fits))

    return

def arith_frame(input_fits1, operand, input_fits2, output_fits):
# initialize
    operand_choices = ('add', 'sub', 'mul', 'div')
    if operand not in operand_choices:
        print('Operand is invalid. Quit this process.')
        return 1

# input fits file1
    if os.path.exists(input_fits1):
        data_in1, header = fits.getdata(input_fits1, view=np.ndarray, header=True)
    else:
        print('Input_file1 does not exist. Quit this process.')
        return 1

# set data_in2
    if os.path.exists(input_fits2):
        data_in2 = fits.getdata(input_fits2, view=np.ndarray)
    elif input_fits2.isnumeric():
        data_in2 = float(input_fits2)
    else:
        print('Input_file2 does not exist. Quit this process.')
        return 1

# check operand
    if operand == operand_choices[0]:
        data_out = data_in1 + data_in2
        operand_sign = '+'
    elif operand == operand_choices[1]:
        data_out = data_in1 - data_in2
        operand_sign = '-'
    elif operand == operand_choices[2]:
        data_out = data_in1 * data_in2
        operand_sign = '*'
    elif operand == operand_choices[3]:
        with np.errstate(divide='ignore', invalid='ignore'):
            data_out = np.divide(data_in1, data_in2)
        operand_sign = '/'

# output
    fits.writeto(output_fits, data_out, header, overwrite = True)
    print('{} {} {} --> {}'.format(input_fits1, operand_sign, input_fits2, output_fits))

    return

def remove_cosmic(input_fits, output_fits):
# input fits file1
    if os.path.exists(input_fits):
        data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)
    else:
        print('Input_file1 does not exist. Quit this process.')
        return 1

# astroscrappy
    crmask, cleanarr = astroscrappy.detect_cosmics(data_in)

# output
    fits.writeto(output_fits, cleanarr, header, overwrite=True)
    print('{} --> {}'.format(input_fits, output_fits))

    return

def calc_gain_ratio(input_fits):
# set constants
    mean_x_pix = ((510, 512, 512, 514), (1022, 1024, 1024, 1026), (1534, 1536, 1536, 1538))
    mean_y_pix = ((400, 1200))

    gain_list = [1, 1, 1.55, 1]

# input fits file
    data_in, header = fits.getdata(input_fits, view = np.ndarray, header = True)

# calculate total counts at regions of channel edges
    ret_list = []

    for tmp_mean_x in mean_x_pix:
        tmp_data1 = data_in[mean_y_pix[0]:mean_y_pix[1], tmp_mean_x[0]:tmp_mean_x[1]]
        tmp_data2 = data_in[mean_y_pix[0]:mean_y_pix[1], tmp_mean_x[2]:tmp_mean_x[3]]

        ret_list.append(np.sum(tmp_data1) / np.sum(tmp_data2))
#    print(tmp_mean)

# calculate gain ratios
    gain_list[0] = float(gain_list[2] / (ret_list[0] * ret_list[1]))
    gain_list[1] = float(gain_list[2] / ret_list[1])
    gain_list[3] = float(gain_list[2] * ret_list[2])

# output gains
    print(gain_list)
    return gain_list

def adjust_gain(input_fits, output_fits, gain_file):
# read the gain file
    with open(gain_file, 'r') as fin:
        gain_list = [float(i) for i in fin.readline().split(' ')]

# check the gain list length
    if len(gain_list) < 4:
        print('Gain list must have 4 arguments.')
        return 1

# set constants
    len_ch_x = 512
    ch_x_pix = ((0, len_ch_x), (len_ch_x, len_ch_x * 2), (len_ch_x * 2, len_ch_x * 3), (len_ch_x * 3, len_ch_x * 4))

# input fits file
    data_in, header = fits.getdata(input_fits, view = np.ndarray, header = True)
    data_out = data_in

# multiply gain for each channel
    for count, [tmp_x, tmp_gain] in enumerate(zip(ch_x_pix, gain_list)):
        tmp_data = data_in[:, tmp_x[0]:tmp_x[1]] * tmp_gain
        data_out[:, (len_ch_x * count):(len_ch_x * (count + 1))] = tmp_data

# output frame
    fits.writeto(output_fits, data_out, header, overwrite = True)
    print('{} --> {}'.format(input_fits, output_fits))

    return
