#!/usr/bin/python3
# coding: utf-8

### data_red/quick_image.py: define functions for making an image quickly

### import libraries
from astropy.io import fits
import numpy as np

from koolsred import const_var
from koolsred.data_red import (extract_spec, data_func)

### set constants

### define functions
def makeImage(obj_frame, ref_frame, roi_pos, roi_size):
    data_obj, header = fits.getdata(obj_frame, view=np.ndarray, header=True)
    data_ref = fits.getdata(ref_frame, view=np.ndarray)

# measure the peak positions
    peak_y_list = extract_spec.findPeakY(data_ref, roi_pos, roi_size)

# measure the peak counts
    start_x, end_x = int(roi_pos[0]), int(roi_pos[0] + roi_size[0])
    data_cut = data_obj[:, start_x:(end_x + 1)].sum(axis=1)
    combine_y_width_half = 2.5
    peak_count_list = [data_func.integral_linear_1d(data_cut, tmp_y, combine_y_width_half) for tmp_y in peak_y_list]

# make an image
    output_name = obj_frame.replace('.fits', '-image-{}-{}.fits'.format(start_x, end_x))

    image_size = 25
    output_data = np.zeros((image_size, image_size))

    for count in range(const_var.n_fiber):
        output_data[const_var.fiber_pos_y[count + 1] - 1][const_var.fiber_pos_x[count + 1] - 1] = peak_count_list[count]

    fits.writeto(output_name, output_data, overwrite = True)
    print('{} --> {}'.format(obj_frame, output_name))
