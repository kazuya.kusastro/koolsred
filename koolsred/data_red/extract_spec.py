#!/usr/bin/python3
# coding: utf-8

### data_red/extract_spec.py: define data reduction functions for spectrum extraction

### import libraries
from astropy.io import fits
import math
import numpy as np
from multiprocessing import Pool
import operator
from scipy import interpolate, ndimage, signal
import time

from koolsred import const_var
from koolsred.data_red import data_func

### define constants
combine_y_half = 1

### define functions
def find_peak_xy2(data, fiberbundle_id='2014'):
    init_x = 5
    step_x = 10
    n_spec = const_var.n_fiber_dict[fiberbundle_id] - len(const_var.skip_fiber_list)

    peak_xy_list = [[] for i in range(n_spec)]

# main loop
    for tmp_x in range(init_x, 2000, step_x):
        center_x = tmp_x + step_x / 2

# find peak and exclude fainter ones
        selected = data[:, tmp_x:(tmp_x + step_x)]
        data_1d = selected.sum(axis=1)
        peak_y = signal.argrelmax(data_1d, order=2)
        peak_y_mean = [(tmp_y, data_1d[tmp_y]) for tmp_y in peak_y[0]]

# sort data with column 1, i.e., peak counts
        sort_column = operator.itemgetter(1)
        peak_sort = sorted(peak_y_mean, key=sort_column, reverse=True)
        peak_y_int = [tmp_y[0] for tmp_y in peak_sort[:n_spec]]
        peak_y_int.sort()
#  print(peak_y_int)
        peak_y_float = findPeakY_float(data, peak_y_int)

        for count, tmp_y in enumerate(peak_y_float):
            peak_xy_list[count].append([center_x, tmp_y])

# interporate fiber positions for skipped fibers
    for tmp_fiber in const_var.skip_fiber_list:
        tmp_pos_list = [[i[0], (i[1] + j[1]) / 2] for (i, j) in zip(peak_xy_list[tmp_fiber - 2], peak_xy_list[tmp_fiber - 1])]

        peak_xy_list.insert(tmp_fiber - 1, tmp_pos_list)

#  print(peak_xy_list)
    return peak_xy_list

def findPeak_XY(data, roi_pos, roi_size):
# initialize
    roi_pos_y_int = int(roi_pos[1])
    init_x = int(roi_pos[0] + roi_size[0] / 2)
    step_x = 10
    combine_x_half = int(step_x / 2)

    peak_y_list = []
    peak_xy_list = []

    selected = data[int(roi_pos[1]):int(roi_pos[1] + roi_size[1]), int(roi_pos[0]):int(roi_pos[0] + roi_size[0])]
    data_1d = selected.sum(axis=1)
    peak_y_int = [i + roi_pos_y_int for i in findPeakY_int(data_1d)]

## measure peak positions along the X-axis
# define a reduction sequence
    def seq_peak_Y():
        if len(tmp_peak_xy) == 0:
            tmp_peak_y = init_y
        else:
            tmp_peak_y = tmp_peak_xy[-1][1]

        tmp_peak_y_int = int(tmp_peak_y + 0.5)
        data_1d = np.sum(data[(tmp_peak_y_int - combine_y_half):(tmp_peak_y_int + combine_y_half + 1), (tmp_x - combine_x_half):(tmp_x + combine_x_half + 1)], axis=1)
#      print(data_func.grav_center2_1d(data_1d))
#    return tmp_peak_y_int - combine_y_half + data_func.grav_center2_1d(data_1d)
        return tmp_peak_y_int - combine_y_half + ndimage.measurements.center_of_mass(data_1d**2)[0]

# main loop
    for init_y in peak_y_int:
        tmp_peak_xy = []
        tmp_x = init_x

# find peaks for -x direction
        while tmp_x - combine_x_half > 0:
            tmp_peak_xy.append([tmp_x, seq_peak_Y()])
            tmp_x -= step_x

# reverse the list
        tmp_peak_xy.reverse()
        tmp_x = init_x + step_x

# find peaks for +x direction
        while tmp_x + combine_x_half < const_var.n_pixel[1]:
            tmp_peak_xy.append([tmp_x, seq_peak_Y()])
            tmp_x += step_x

        peak_xy_list.append(tmp_peak_xy)

    return peak_xy_list

def findPeakY(data, roi_pos, roi_size):
    selected = data[int(roi_pos[1]):int(roi_pos[1] + roi_size[1]), int(roi_pos[0]):int(roi_pos[0] + roi_size[0])]
    data_1d = selected.sum(axis=1)

    peak_y_int = findPeakY_int(data_1d)
    peak_y_float = findPeakY_float(data_1d, peak_y_int)

    roi_pos_y_int = int(roi_pos[1])
    return [i + roi_pos_y_int for i in peak_y_float]

def findPeakY_int(mean):
# find peak and exclude fainter ones
#  selected = data[int(roi_pos[1]):int(roi_pos[1] + roi_size[1]), int(roi_pos[0]):int(roi_pos[0] + roi_size[0])]
#  mean = selected.mean(axis=1)
    peak_y = signal.argrelmax(mean, order=2)
    peak_y_mean = [(tmp_y, mean[tmp_y]) for tmp_y in peak_y[0]]

# sort data with column 1, i.e., peak counts
    sort_column = operator.itemgetter(1)
    peak_sort = sorted(peak_y_mean, key=sort_column, reverse=True)
#  roi_pos_y_int = int(roi_pos[1])
#  peak_y_int = [tmp_y[0] + roi_pos_y_int for tmp_y in peak_sort[:const_var.n_fiber]]
    peak_y_int = [tmp_y[0] for tmp_y in peak_sort[:const_var.n_fiber]]
    peak_y_int.sort()
#  print(peak_y_int)

    return peak_y_int

def findPeakY_float(data, peak_y_int):
    peak_y_list = []

    for tmp_peak_y in peak_y_int:
        data_cut = data[(tmp_peak_y - combine_y_half):(tmp_peak_y + combine_y_half + 1)]
#    peak_y_list.append(tmp_peak_y - combine_y_half + data_func.grav_center2_1d(mean_cut))
        peak_y_list.append(tmp_peak_y - combine_y_half + ndimage.measurements.center_of_mass(data_cut**2)[0])

    return peak_y_list

def importPeakFile(peak_file):
    with open(peak_file, 'r') as fin:
        peak_whole = fin.readlines()

    peak_list = []
    for tmp_fiber in peak_whole:
        tmp_split = tmp_fiber.rstrip().split(' ')
        tmp_list = [[float(tmp_split[i]), float(tmp_split[i+1])] for i in range(0, len(tmp_split), 2)]

# sort in x
        sort_column = operator.itemgetter(0)
        tmp_list = sorted(tmp_list, key=sort_column)

        peak_list.append(tmp_list)

    return peak_list

def extract_fits(input_fits, output_fits, peak_file, num_cpu=1):
# initialize
#  start_time = time.time()
#  max_x_out = const_var.n_pixel[1]
#  pos_y_array = range(const_var.n_pixel[0])

    data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)

# input the peak pos file
    peak_list_orig = importPeakFile(peak_file)

# check the number of fibers
    n_fiber = len(peak_list_orig)
    if not n_fiber in const_var.n_fiber_dict.values():
        print('The peak_pos file is invalid.')
        return

    data_out = np.zeros((n_fiber, const_var.n_pixel[1]))

# extrapolate peaks near the edge
    peak_list = []
    for tmp_list in peak_list_orig:
        if tmp_list[0][0] > 0:
            x1, y1, x2, y2 = tmp_list[0][0], tmp_list[0][1], tmp_list[1][0], tmp_list[1][1]
            tmp_list.insert(0, [0, y1 - (y2 - y1) / (x2 - x1) * x1])

        if tmp_list[-1][0] < const_var.n_pixel[1]:
            x1, y1, x2, y2 = tmp_list[-2][0], tmp_list[-2][1], tmp_list[-1][0], tmp_list[-1][1]
            tmp_list.append([const_var.n_pixel[1], y2 + (y2 - y1) / (x2 - x1) * (const_var.n_pixel[1] - x2)])

        peak_list.append(tmp_list)

# calculate fluxes with multiprocessing
# with multiprocessing.Pool
    input_args = [[i, data_in, peak_list[i]] for i in range(n_fiber)]

    if num_cpu == 1:
        data_out = list(map(calc_flux_wrapper, input_args))

    else:
        with Pool(processes=num_cpu) as pool:
            data_out = pool.map(calc_flux_wrapper, input_args)

#    data_out = calc_flux2(data_in, peak_list)

    fits.writeto(output_fits, data_out, header, overwrite=True)
    print('{} --> {}'.format(input_fits, output_fits))

    return

# function for extractSpec
def calc_flux_wrapper(input_args):
    return calc_flux(*input_args)

def calc_flux(count_fiber, data_in, peak_list):
#  count_fiber, data_in, peak_list = input_args
    combine_y_width_half = 2.5

#  print('count_fiber = {}'.format(count_fiber))

    tmp_array = np.array(peak_list)
    peak_x = tmp_array[:, 0]
    peak_y = tmp_array[:, 1]

    func_interp = interpolate.interp1d(peak_x, peak_y)

    def calc_flux_in(tmp_x):
        center_y = func_interp(tmp_x)
        min_y, max_y = center_y - combine_y_width_half, center_y + combine_y_width_half
        return data_func.calc_flux(data_in[:, tmp_x], min_y, max_y)

    return [calc_flux_in(i) for i in range(const_var.n_pixel[1])]

def calc_flux2(data_in, peak_list):
# measure elapsed time
    time_start = time.time()

# define constants
    comb_y_half = 2.5
    n_fiber = const_var.n_pixel[1]
    size_x = data_in.shape[1]

    data_out = np.zeros((n_fiber, size_x))

# loop_x
    for tmp_fiber, tmp_peak in enumerate(peak_list):

# input the peak list
        tmp_array = np.array(tmp_peak)
        peak_x = tmp_array[:, 0]
        peak_y = tmp_array[:, 1]

        func_interp = interpolate.interp1d(peak_x, peak_y)

        for tmp_x in range(size_x):
            center_y = func_interp(tmp_x)
            min_y = center_y - comb_y_half
            max_y = center_y + comb_y_half
            tmp_array = data_in[int(min_y):int(max_y + 1), tmp_x]

            weight_array = np.ones(np.size(tmp_array))
            if np.size(weight_array) != comb_y_half * 2:
                weight_array[0] = 1 - (min_y - int(min_y))
                weight_array[-1] = max_y - int(max_y)

            data_out[tmp_fiber][tmp_x] = np.sum(tmp_array * weight_array)

#        print('Elapsed time = {:.3f} sec.'.format(time.time() - time_start))

    return data_out
