#!/usr/bin/python3
# coding: utf-8

### data_red/no_grism_image.py: define data reduction functions for reconstrcuting images from frames without a grism

### import libraries
from astropy.io import fits
import numpy as np
import operator
import os
from scipy import signal

from koolsred import const_var
#from koolsred.data_red import data_func, FiberAlignment
from koolsred.data_red import data_func

### define functions
def findPeak_XY(data, roi_pos, roi_size, fiber_id='2014'):
# initialize
    combine_half_int = 2

## find peak positions in Y-axis
# find peak and exclude fainter ones
    selected = data[int(roi_pos[1]):int(roi_pos[1] + roi_size[1]), int(roi_pos[0]):int(roi_pos[0] + roi_size[0])]
    mean = selected.mean(axis=1)
    peak_y = signal.argrelmax(mean, order=2)
    peak_y_mean = [(tmp_y, mean[tmp_y]) for tmp_y in peak_y[0]]

# sort data with column 1, i.e., peak counts
    sort_column = operator.itemgetter(1)
    peak_sort = sorted(peak_y_mean, key=sort_column, reverse=True)
    peak_y_int = [tmp_y[0] for tmp_y in peak_sort[:const_var.n_fiber[fiber_id]]]
    peak_y_int.sort()
#  print(peak_y_init)

## find peak positions in X-axis
    peak_x_int = []
    for init_y in peak_y_int:
#    print(int(roi_pos[1] + init_y - combine_half_int), int(roi_pos[1] + init_y + combine_half_int + 1), int(roi_pos[0]), int(roi_pos[0] + roi_size[0]))
        selected = data[int(roi_pos[1] + init_y - combine_half_int):int(roi_pos[1] + init_y + combine_half_int + 1), int(roi_pos[0]):int(roi_pos[0] + roi_size[0])]

        peak_x_int.append(selected.mean(axis=0).argmax())

## measure peak positions along the X-axis
    peak_xy_list = []
    for init_x, init_y in zip(peak_x_int, peak_y_int):
        selected = data[int(roi_pos[1] + init_y - combine_half_int):int(roi_pos[1] + init_y + combine_half_int + 1), int(roi_pos[0] + init_x - combine_half_int):int(roi_pos[0] + init_x + combine_half_int + 1)]
        peak_x = roi_pos[0] + init_x - combine_half_int + data_func.grav_center2_1d(selected.mean(axis=0))
        peak_y = roi_pos[1] + init_y - combine_half_int + data_func.grav_center2_1d(selected.mean(axis=1))

        peak_xy_list.append([peak_x, peak_y])

    return peak_xy_list

def measureCount(data, peak_list):
    width_half = [3, 3]
    peak_count = [data_func.integral_linear_2d(data, i, width_half) for i in peak_list]

    return peak_count

def reconstruct_image(input_fits, output_fits, peak_file, norm_flag):
# check file
    if not os.path.isfile(input_fits):
        print('The input fits file is not found.')
        return 1

    if not os.path.isfile(peak_file):
        print('The peak position file is not found.')
        return 1

# input the peak position file
    with open(peak_file, 'r') as fin:
        file_lines = fin.readlines()

    peak_pos = []
    peak_norm = []
    for line in file_lines:
        line_split = line.split(' ')
        peak_pos.append([float(line_split[0]), float(line_split[1])])
        peak_norm.append(float(line_split[2]))

#  print(peak_pos, peak_norm)

# input the fits file
    data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)

# check the fiber version
    fiber_id = header['FIBER-ID']
    if not fiber_id in n_fiber_dict:
        print('{} is not in fiber ID list.'.format(fiber_id))
        return

    fiber_pos_x = const_var.fiber_pos_x_dict[fiber_id]
    fiber_pos_y = const_var.fiber_pos_y_dict[fiber_id]
    tmp_size = np.shape(fiber_pos_2d_dict[fiber_id])
    data_out = np.zeros((tmp_size[1], tmp_size[0]))

# calculate peak counts
    peak_count = measureCount(data_in, peak_pos)
    if norm_flag:
        peak_count = [i / j for i, j in zip(peak_count, peak_norm)]

# make the reconstruct image
    for count_fiber, tmp_count in enumerate(peak_count):
        data_out[fiber_pos_y[count_fiber] - 1, fiber_pos_x[count_fiber] - 1] = tmp_count

# output the fits file
    fits.writeto(output_fits, data_out, header, overwrite=True)
    print('{} --> {}.'.format(input_fits, output_fits))
