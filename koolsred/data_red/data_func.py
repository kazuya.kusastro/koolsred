#!/usr/bin/python3
# coding: utf-8

### data_red/data_func.py: define common functions for data reduction

### import libraries
from astropy.io import fits
import math
import numpy as np
import os
import warnings

from koolsred import const_var
#from koolsred.data_red import FiberAlignment

## functions for 2D fits
def arith_frame(input_fits1, operand, input_fits2, output_fits):
# initialize
    operand_choices = ('add', 'sub', 'mul', 'div')
    if operand not in operand_choices:
        print('Operand is invalid. Quit this process.')
        return 1

# input fits file1
    if os.path.exists(input_fits1):
        data_in1, header = fits.getdata(input_fits1, view = np.ndarray, header = True)
    else:
        print('Input_file1 does not exist. Quit this process.')
        return 1

# set data_in2
    if os.path.exists(input_fits2):
        data_in2 = fits.getdata(input_fits2, view = np.ndarray)
    elif input_fits2.isnumeric():
        data_in2 = float(input_fits2)
    else:
        print('Input_file2 does not exist. Quit this process.')
        return 1

# check operand
    if operand == operand_choices[0]:
        data_out = data_in1 + data_in2
        operand_sign = '+'
    elif operand == operand_choices[1]:
        data_out = data_in1 - data_in2
        operand_sign = '-'
    elif operand == operand_choices[2]:
        data_out = data_in1 * data_in2
        operand_sign = '*'
    elif operand == operand_choices[3]:
        with np.errstate(divide='ignore'):
            data_out = np.nan_to_num(np.divide(data_in1, data_in2))
        operand_sign = '/'

# output
    fits.writeto(output_fits, data_out, header, overwrite = True)
    print('{} {} {} --> {}'.format(input_fits1, operand_sign, input_fits2, output_fits))

    return

## functions for spectrum
def comb_spec_all(input_fits, output_fits):
    data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)
    data_out = data_in.sum(axis=0)

    fits.writeto(output_fits, data_out, header, overwrite=True)
    print('{} --> {}'.format(input_fits, output_fits))

    return

def subtract_sky(input_fits, output_fits):
    data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)
    if '2019' in header['FIBER-ID']:
        data_out = subtract_sky_2019(data_in)
    else:
        data_out = subtract_sky_2014(data_in)

    fits.writeto(output_fits, data_out, header, overwrite=True)
    print('{} --> {}'.format(input_fits, output_fits))

    return

def subtract_sky_2014(data_in):
    data_out = data_in

# make sky spectrum
    sky_fiber_id = np.argsort(data_in[:, 200:-300].sum(axis=1))[0:10].tolist()
#  with np.errstate(invalid='ignore'):
    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', r'Invalid value encountered in median')
        sky_spec = np.median(data_in[sky_fiber_id, :], axis=0)

# subtract sky spectrum
    for tmp_y in range(const_var.n_fiber_dict['2014']):
        data_out[tmp_y, :] = data_in[tmp_y, :] - sky_spec

    return data_out

def subtract_sky_2019(data_in):
    data_in[np.isnan(data_in)] = 0
    data_in[np.isinf(data_in)] = 0

    data_out = data_in
    data_sky = np.delete(data_in, slice(78, 84), 0)

# make sky spectrum
    sky_fiber_id = np.argsort(data_sky[:, 200:-300].sum(axis=1))[0:10].tolist()
    sky_spec = np.median(data_in[sky_fiber_id, :], axis=0)

# subtract sky spectrum
    data_sky = np.array([sky_spec] * const_var.n_fiber_dict['2019'])
    data_out = np.subtract(data_in, data_sky, where=sky_spec!=np.nan)
#  for tmp_y in range(const_var.n_fiber_dict['2019']):
#    data_out[tmp_y, :] = data_in[tmp_y, :] - sky_spec

    return data_out

def comb_spec(input_fits, output_fits, num_comb_fiber):
#  num_comb_fiber = 15

    data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)

# make object spectrum
    obj_fiber_id = np.argsort(data_in[:, 1100:1200].sum(axis=1)).tolist()
    data_out = np.sum(data_in[obj_fiber_id[-num_comb_fiber:-1], :], axis=0)

#  print(data_in[:, 1100:1200].sum(axis=1))
#  print(obj_fiber_id)

    fits.writeto(output_fits, data_out, header, overwrite=True)
    print('{} --> {}'.format(input_fits, output_fits))

    return

## functions for making images
def make_2d_image(input_fits, output_fits, start_x, end_x):
# input the fits file
    data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)

# check the fiber version
    fiber_id = header['FIBER-ID']
    if not fiber_id in const_var.n_fiber_dict:
        print('{} is not in fiber ID list.'.format(fiber_id))
        return

    n_fiber = const_var.n_fiber_dict[fiber_id]
    fiber_pos_x = const_var.fiber_pos_x_dict[fiber_id]
    fiber_pos_y = const_var.fiber_pos_y_dict[fiber_id]
    tmp_size = np.shape(const_var.fiber_pos_2d_dict[fiber_id])
    data_out = np.zeros((tmp_size[0], tmp_size[1]))

# make the reconstruct image
    for count_fiber in range(n_fiber):
        data_out[fiber_pos_y[count_fiber] - 1, fiber_pos_x[count_fiber] - 1] = data_in[count_fiber, start_x:end_x].sum()

# fill 2x2 pixels for a fiber
    if fiber_id == '2019':
        data_roll_x = np.roll(data_out, 1, axis=1)
        data_roll_y = np.roll(data_out, 1, axis=0)
        data_roll_xy = np.roll(data_out, (1, 1), axis=(0, 1))
        data_out = data_out + data_roll_x + data_roll_y + data_roll_xy

# output the fits file
    fits.writeto(output_fits, data_out, header, overwrite=True)

    return

def mask_2d_image(input_fits):
    data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)
    fiber_id = header['FIBER-ID']

    if fiber_id == '2019':
        fiber_pos_2d = const_var.fiber_pos_2d_dict['2019']
        roll_x = np.roll(fiber_pos_2d, 1, axis=1)
        roll_y = np.roll(fiber_pos_2d, 1, axis=0)
        roll_xy = np.roll(fiber_pos_2d, (1, 1), axis=(0, 1))
        fiber_pos_2d_roll = fiber_pos_2d + roll_x + roll_y + roll_xy
        return np.ma.masked_where(fiber_pos_2d_roll == 0, data_in)
    else:
        return np.ma.masked_where(False, data_in)

def make_3d_image(input_fits):
# input file
    if not os.access(input_fits, os.R_OK):
        print('Cannot open %s.'.format(input_fits))
        return

    data_in, header_in = fits.getdata(input_fits, view=np.ndarray, header=True)

# check the fiber version
    fiber_id = header_in['FIBER-ID']
    if not fiber_id in const_var.n_fiber_dict.keys():
        print('{} is not in fiber ID list.'.format(fiber_id))
        return

    fiber_pos_x = const_var.fiber_pos_x_dict[fiber_id]
    fiber_pos_y = const_var.fiber_pos_y_dict[fiber_id]
    tmp_size = np.shape(const_var.fiber_pos_2d_dict[fiber_id])

# prepare for an output file
#  out_img_size = 25
    out_spec_size = int(header_in['NAXIS1'])
    n_fiber = int(header_in['NAXIS2'])

    output_fits = input_fits.replace('.fits', '-3d.fits')

# make output data
    data_out = np.zeros((out_spec_size, tmp_size[0], tmp_size[1] + 1))
    for count in range(n_fiber):
#    print('count = {} pos_y = {} pos_x = {}'.format(count, fiber_pos_y[count], fiber_pos_x[count]))
        data_out[:, fiber_pos_y[count] - 1, fiber_pos_x[count] - 1] = data_in[count, :]

# fill 2x2 pixels for a fiber
    if fiber_id == '2019':
        data_roll_x = np.roll(data_out, 1, axis=2)
        data_roll_y = np.roll(data_out, 1, axis=1)
        data_roll_xy = np.roll(data_out, (1, 1), axis=(1, 2))
        data_out = data_out + data_roll_x + data_roll_y + data_roll_xy

# make output header
    header_out = header_in
    header_out['CRVAL1'] = 1
    header_out['CDELT1'] = 1
    header_out['CD1_1'] = 1

    header_out['CRVAL3'] = header_in['CRVAL1']
    header_out['CDELT3'] = header_in['CDELT1']
    header_out['CD3_3'] = header_in['CD1_1']

# output fits
    fits.writeto(output_fits, data_out, header_out, overwrite=True)
    print(input_fits, '->', output_fits)

    return

## functions for data reduction
def grav_center2_1d(data):
    pos_array = np.array([i for i in range(data.size)])
    data2 = data**2

    count_sum = np.sum(data2)
    pos_count_sum = np.sum(data2 * pos_array)

    return pos_count_sum / count_sum

def calc_flux(data, min_pix, max_pix):
    if max_pix < 0 or min_pix > data.size or min_pix > max_pix:
        return 0

    min_pix = max([min_pix, 0])
    max_pix = min([max_pix, data.size - 1])

    min_int, max_int = math.floor(min_pix), math.floor(max_pix)
    min_float, max_float = min_pix - min_int, max_pix - max_int

    if min_int == max_int:
        return data[min_int] * (max_float - min_float)
    elif max_int - min_int == 1:
        return data[min_int] * (1 - min_float) + data[max_int] * max_float
    else:
        return data[min_int] * (1 - min_float) + np.sum(data[min_int + 1: max_int]) + data[max_int] * max_float


def integral_linear_1d(data, center, width_half):
# calculate int and float of the range values
    min_x, max_x = center - width_half, center + width_half
    min_int, max_int = math.floor(min_x), math.floor(max_x)
    min_float, max_float = min_x - min_int, max_x - max_int

# make the mask vector
    if min_int == max_int:
        mask = [max_float - min_float]
    else:
        mask = [1 - min_float] + [1] * (max_int - min_int - 1) + [max_float]

    data_cut = data[min_int:(max_int + 1)]
    return (data_cut * mask).sum()

def integral_linear_2d(data, center, width_half):
# initialize
    tmp_mask = []

# calculate int and float of the range values
    min_xy = [i - j for i, j in zip(center, width_half)]
    max_xy = [i + j for i, j in zip(center, width_half)]
    min_int = [math.floor(i) for i in min_xy]
    max_int = [math.floor(i) for i in max_xy]
    min_float = [i - j for i, j in zip(min_xy, min_int)]
    max_float = [i - j for i, j in zip(max_xy, max_int)]

# make the mask matrix
    for count in range(len(center)):
        if min_int[count] == max_int[count]:
            tmp_mask.append([max_float - min_float])
        else:
            tmp_mask.append([1 - min_float[count]] + [1] * (max_int[count] - min_int[count] - 1) + [max_float[count]])

    mask1 = np.tile(np.array(tmp_mask[0]), (len(tmp_mask[1]), 1))
    mask2 = np.tile(np.array(tmp_mask[1]), (len(tmp_mask[0]), 1)).T
#  mask12 = mask1 * mask2

#  print(center, mask12)
    data_cut = data[min_int[1]:(max_int[1] + 1), min_int[0]:(max_int[0] + 1)]
#  print(data_cut * mask1 * mask2)

    return (data_cut * mask1 * mask2).sum()


#def grav_center(data, pos_x, pos_y, width, height):
#  height_half = int((height - 1) / 2)
#  start_x, end_x = int(pos_x), int(pos_x) + int(width)
#  start_y, end_y = int(pos_y) - height_half, int(pos_y) + height_half + 1
#  cut_array = data[start_y:end_y, start_x:end_x]
#
#  count_sum = np.sum(cut_array)
#  count_sum_x = np.sum(cut_array, axis=1)
#  pos_y_array = [i - height_half for i in range(height)]
#  pos_count_sum = np.sum(count_sum_x * pos_y_array)
#
#  if count_sum < 0:
#    weight = 0
#  elif count_sum > 1:
#    weight = 1
#  else:
#    weight = count_sum
#
#  return [pos_count_sum / count_sum + pos_y, weight]

## utility
def checkFiberID(fiberID, fiberbundle_id='2014'):
    fiber_int = int(fiberID)
    n_fiber = const_var.n_fiber_dict[fiberbundle_id]

    if fiber_int < 1:
        return 1
    elif fiber_int > n_fiber:
        return n_fiber
    else:
        return fiber_int
