#!/usr/bin/python3
# coding: utf-8

### data_red/wave_calib.py: define functions for wavelength calibration

### import libraries
from astropy.io import fits
import math
import numpy as np
import operator
import os
from scipy import (signal, interpolate, integrate)
import sys
import time

# KOOLS reduction modules
from koolsred import const_var

### define constants
fiber_gap_id_dict = {'2014': [42, 85], '2019': [39, 78]}

### define functions
def measurePeak(data, init_x):
    cut1_half, cut2_half = 5, 3

    cut1_array = data[(init_x - cut1_half):(init_x + cut1_half + 1)]
    peak1_x = init_x - cut1_half + np.argmax(cut1_array)
    cut2_array = np.array(data[(peak1_x - cut2_half):(peak1_x + cut2_half + 1)])

    return peak1_x - cut2_half + ((cut2_array * np.arange(cut2_array.size)).sum() / cut2_array.sum())

def markPeakAll(data, init_x, init_y, fiberbundle_id='2014'):
# include fiberbundle ID
    fiber_gap_id = fiber_gap_id_dict[fiberbundle_id]
    n_fiber = const_var.n_fiber_dict[fiberbundle_id]

# find the initial peak
#  peak_x_init = measurePeak(data[:, init_y], init_x)
#  peak_xy = [[peak_x_init, init_y]]

# find peaks in the other fibers
#  prev_x = init_x
    peak_xy = [[init_x, init_y]]

    if init_y in fiber_gap_id:
        start_y = init_y + 1
    else:
        start_y = init_y

# descending order in the fiber ID
    for tmp_y in range(start_y - 1, -1, -1):
# skip fiber
        if tmp_y in const_var.skip_fiber_list:
            continue

# at the large fiber gap, change the tmp_x
        if tmp_y - 1 in fiber_gap_id:
            tmp_x = int(peak_xy[-1][0] + 14.4 * (peak_xy[-1][0] - peak_xy[-2][0]))
# otherwise, tmp_x = prev_x
        else:
            tmp_x = int(peak_xy[-1][0])

        peak_x = measurePeak(data[tmp_y, :], tmp_x)
        peak_xy.append([peak_x, tmp_y])

# ascending order in the fiber ID
    peak_xy.reverse()

    for tmp_y in range(start_y + 1, n_fiber):
# skip fiber
        if tmp_y in const_var.skip_fiber_list:
            continue

# at the large fiber gap, change the tmp_x
        if tmp_y in fiber_gap_id:
            tmp_x = int(peak_xy[-1][0] + 14.4 * (peak_xy[-1][0] - peak_xy[-2][0]))
# otherwise, tmp_x = prev_x
        else:
            tmp_x = int(peak_xy[-1][0])

        peak_x = measurePeak(data[tmp_y, :], tmp_x)
        peak_xy.append([peak_x, tmp_y])

# sort the list in the order of the fiber ID
    sort_column = operator.itemgetter(1)
    peak_xy_list = sorted(peak_xy, key=sort_column)

# interporate fiber positions for skipped fibers
    for tmp_fiber in const_var.skip_fiber_list:
        tmp_peak_x = (peak_xy_list[tmp_fiber - 1][0] + peak_xy_list[tmp_fiber][0]) / 2
        peak_xy_list.insert(tmp_fiber, [tmp_peak_x, tmp_fiber])

    return peak_xy_list

def fitResidual(input_array):
    fit_order = 4

    fit_x = input_array[:, 0].astype(np.float32)
    fit_y = input_array[:, 1].astype(np.float32)
    array_size = fit_x.size

    f_cheby = np.polynomial.Chebyshev.fit(fit_x, fit_y, fit_order)
    resi_array = np.array([tmp_y - f_cheby(tmp_x) for tmp_x, tmp_y in zip(fit_x, fit_y)])
#  print(resi_array)

    ret_array = np.stack((fit_x.T, resi_array.T), axis=1)
#  print(ret_array)

    return ret_array, f_cheby

def wave_calib(input_fits, output_fits, grism, pos_lam_file):
# get grism ID
    grism_names_lower = [i.lower() for i in const_var.grism_names]

    if grism in grism_names_lower:
        grism_id = grism_names_lower.index(grism)
    else:
        print('Grism name does not match.')
        return

# read pos_lam file
    pos_lam_list = []
    with open(pos_lam_file, 'r') as fin:
        line = fin.readline()

        while line:
            line_split = line.split(' ')
            tmp_list = []
            while len(line_split) >= 4:
                tmp_list.append([float(line_split[1]), float(line_split[2])])
                del line_split[0:3]

            pos_lam_list.append(tmp_list)
            line = fin.readline()

# define the wavelength calibration setting: start_lambda, step_lambda, n_lambda_pixel
    wave_config_whole = [[4000, 2, 2500], [5500, 2, 2500], [4000, 1, 2000], [5500, 1, 2500]]
    wave_config = wave_config_whole[grism_id]
    n_fiber = len(pos_lam_list)

    data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)
    data_out = np.zeros((n_fiber, wave_config[2]))

    header['CRPIX1'] = 1
    header.comments['CRPIX1'] = 'Starting pixel'
    header['CRVAL1'] = wave_config[0] + wave_config[1]
    header.comments['CRVAL1'] = 'Centeral wavelength of the first pixel'
    header['CD1_1'] = wave_config[1]
    header.comments['CD1_1'] = 'Linear dispersion per pixel'
    header['CTYPE1'] = 'WAVE'
    header.comments['CTYPE1'] = 'Name of the coordinate axis'

    def calc_flux(data, f_cheby, start_lam, delta_lam):
# calculate the position at ...-extract.fits.
# note: start_lam corresponds to max_pix
        max_pix = f_cheby(start_lam)
        min_pix = f_cheby(start_lam + delta_lam)

        if max_pix < 0 or min_pix > data.size or min_pix > max_pix:
            return 0
        else:
            min_pix = max([min_pix, 0])
            max_pix = min([max_pix, data.size - 1])

            min_int, max_int = math.floor(min_pix), math.floor(max_pix)
            min_float, max_float = min_pix - min_int, max_pix - max_int

            if min_int == max_int:
                return data[min_int] * (max_float - min_float)
            elif max_int - min_int == 1:
                return data[min_int] * (1 - min_float) + data[max_int] * max_float
            else:
                return data[min_int] * (1 - min_float) + np.sum(data[min_int + 1: max_int]) + data[max_int] * max_float

# wavelength calibration
    fit_order = 3

    for count_fiber in range(n_fiber):
        data_cut = data_in[count_fiber, :]
# fitLine(input_fiber):
#    fit_x = fit_y = []

        tmp_pos_lam = np.array(pos_lam_list[count_fiber])
        fit_x = tmp_pos_lam[:, 1].astype(np.float32)
        fit_y = tmp_pos_lam[:, 0].astype(np.float32)

        f_cheby = np.polynomial.Chebyshev.fit(fit_x, fit_y, fit_order)
        for count_lam in range(wave_config[2]):
            start_lam = wave_config[0] + wave_config[1] * (count_lam - 1)
            data_out[count_fiber, count_lam] = calc_flux(data_cut, f_cheby, start_lam, wave_config[1])

    fits.writeto(output_fits, data_out, header, overwrite=True)

    print('{} --> {}'.format(input_fits, output_fits))

def makeFlat(input_fits, output_fits):
    data_in, header = fits.getdata(input_fits, view=np.ndarray, header=True)

# check the fiber version
    fiberbundle_id = header['FIBER-ID']
    if not fiberbundle_id in const_var.n_fiber_dict.keys():
        print('FIBER-ID is invalid.\nQuit this process.')
        return

    n_fiber = const_var.n_fiber_dict[fiberbundle_id]
    data_out = np.zeros((n_fiber, int(header['NAXIS1'])))

    for tmp_x in range(int(header['NAXIS1'])):
        data_out[:, tmp_x] = np.divide(data_in[:, tmp_x], data_in[:, tmp_x].mean())

    fits.writeto(output_fits, data_out, header, overwrite=True)
    print('{} --> {}'.format(input_fits, output_fits))

    return

def make_twilight_flat(input_list, output_fits):
# make twilight flat
    header = fits.getheader(input_list[0])

# check the fiber version
    fiberbundle_id = header['FIBER-ID']
    if not fiberbundle_id in const_var.n_fiber_dict.keys():
        print('FIBER-ID is invalid.\nQuit this process.')
        return

    n_fiber = const_var.n_fiber_dict[fiberbundle_id]
    naxis1 = int(header['NAXIS1'])

    data_out = np.zeros((len(input_list), n_fiber, naxis1))

    for count, tmp_file in enumerate(input_list):
        data_in = fits.getdata(tmp_file)
        for tmp_x in range(naxis1):
            data_out[count, :, tmp_x] = np.divide(data_in[:, tmp_x], data_in[:, tmp_x].mean())

    data_out = np.median(data_out, axis=0)   
    fits.writeto(output_fits, data_out, header, overwrite=True)
    print('{} --> {}'.format(input_list, output_fits))

    return
