#!/usr/bin/python3
# coding: utf-8

### gui/setting.py: GUI for the setting tab

### import libraries
import PyQt5
from PyQt5.QtWidgets import (QLabel, QLineEdit, QTableWidget, QTableWidgetItem, QPushButton, QComboBox, QRadioButton, QButtonGroup, QFileDialog, QFrame, QGridLayout)
from PyQt5.QtCore import Qt

# KOOLS reduction modules
from koolsred import const_var

### set constants

## define setting box
class boxSetting(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

## define widgets
        miniLabelFrameFormat = QLabel('Frame name format')
        miniLabelFrameFormat.setFont(const_var.font)
        self.comboFrameFormat = QComboBox()
        self.comboFrameFormat.setFont(const_var.font)
        for tmp_name in const_var.frameNameChoices:
            self.comboFrameFormat.addItem(tmp_name)
        self.comboFrameFormat.currentIndexChanged.connect(self.updateFrameFormat)

        miniLabelThreadLimit = QLabel('CPU thread limit')
        miniLabelThreadLimit.setFont(const_var.font)
        self.boxThreadLimit = QLineEdit()
        self.boxThreadLimit.setFont(const_var.font)
        self.boxThreadLimit.setAlignment(Qt.AlignRight)
        self.boxThreadLimit.setMaxLength(2)
        self.boxThreadLimit.setText(str(const_var.cpuThreadLimit))
        self.boxThreadLimit.editingFinished.connect(self.updateThreadLimit)
        self.buttonThreadLimitPlus = QPushButton('+')
        self.buttonThreadLimitPlus.clicked.connect(self.updateThreadLimit)
        self.buttonThreadLimitPlus.setFont(const_var.font)
        self.buttonThreadLimitMinus = QPushButton('-')
        self.buttonThreadLimitMinus.clicked.connect(self.updateThreadLimit)
        self.buttonThreadLimitMinus.setFont(const_var.font)

        miniLabelOverwrite = QLabel('Frame Overwrite')
        miniLabelOverwrite.setFont(const_var.font)
        radioOverwriteYes = QRadioButton('Yes')
        radioOverwriteYes.setFont(const_var.font)
        radioOverwriteYes.setChecked(False)
        radioOverwriteYes.clicked.connect(self.updateFrameOverwrite)
        radioOverwriteNo = QRadioButton('No')
        radioOverwriteNo.setFont(const_var.font)
        radioOverwriteNo.setChecked(True)
        radioOverwriteNo.clicked.connect(self.updateFrameOverwrite)
        self.groupOverwrite = QButtonGroup()
        self.groupOverwrite.addButton(radioOverwriteYes, 1)
        self.groupOverwrite.addButton(radioOverwriteNo, 2)

        mini_label_skip_fiber_id = QLabel('Skip fiber ID')
        mini_label_skip_fiber_id.setFont(const_var.font)
        self.table_skip_fiber_id = QTableWidget(0, 1)
        self.table_skip_fiber_id.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}')

        self.button_skip_sky_fiber = QPushButton('Skip sky fibers')
        self.button_skip_sky_fiber.clicked.connect(self.skip_sky_fibers)
        self.button_skip_sky_fiber.setFont(const_var.font)
        self.button_add_row = QPushButton('Add row')
        self.button_add_row.clicked.connect(self.add_skip_row)
        self.button_add_row.setFont(const_var.font)
        self.button_delete_row = QPushButton('Delete row')
        self.button_delete_row.clicked.connect(self.delete_skip_row)
        self.button_delete_row.setFont(const_var.font)

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

#    layout.addWidget(miniLabelInputPath, 0, 0)
#    layout.addWidget(self.buttonInputPath, 0, 2)
#    layout.addWidget(self.boxInputPath, 1, 0, 1, 3)
#    layout.addWidget(miniLabelOutputPath, 2, 0)
#    layout.addWidget(self.buttonOutputPath, 2, 2)
#    layout.addWidget(self.boxOutputPath, 3, 0, 1, 3)

        layout.addWidget(miniLabelFrameFormat, 0, 0)
        layout.addWidget(self.comboFrameFormat, 0, 1, 1, 3)
        layout.addWidget(miniLabelThreadLimit, 1, 0)
        layout.addWidget(self.boxThreadLimit, 1, 1)
        layout.addWidget(self.buttonThreadLimitPlus, 1, 2)
        layout.addWidget(self.buttonThreadLimitMinus, 1, 3)

        layout.addWidget(miniLabelOverwrite, 2, 0)
        layout.addWidget(radioOverwriteYes, 2, 1)
        layout.addWidget(radioOverwriteNo, 2, 2)

        layout.addWidget(mini_label_skip_fiber_id, 3, 0, 3, 1)
        layout.addWidget(self.table_skip_fiber_id, 3, 1, 3, 1)
        layout.addWidget(self.button_skip_sky_fiber, 3, 2)
        layout.addWidget(self.button_add_row, 4, 2)
        layout.addWidget(self.button_delete_row, 5, 2)

        self.setLayout(layout)

# function
#  def openPath(self):
#    path = QFileDialog.getExistingDirectory(self, 'Open file', './')
#
#    sender = self.sender()
#    if sender is self.buttonInputPath:
#      self.boxInputPath.setText(path)
#      const_var.inputPath = self.boxInputPath.text()
#    elif sender is self.buttonOutputPath:
#      self.boxOutputPath.setText(path)
#      const_var.outputPath = self.boxOutputPath.text()
#
#  def updatePath(self):
#    sender = self.sender()
#    if sender is self.boxInputPath:
#      const_var.inputPath = self.boxInputPath.text()
#    elif sender is self.boxOutputPath:
#      const_var.outputPath = self.boxOutputPath.text()

    def updateFrameFormat(self):
        const_var.frameFormat = self.comboFrameFormat.currentText()

    def updateThreadLimit(self):
        tmp_var = int(self.boxThreadLimit.text())

        sender = self.sender()
        if sender is self.buttonThreadLimitPlus:
            tmp_var += 1
        elif sender is self.buttonThreadLimitMinus:
            tmp_var -= 1

        if tmp_var < 1:
            tmp_var = 1

        self.boxThreadLimit.setText(str(tmp_var))
        const_var.cpuThreadLimit = tmp_var

    def skip_sky_fibers(self):
        row_number_init = self.table_skip_fiber_id.rowCount()
        for count in range(7):
            row_number = row_number_init + count
            self.table_skip_fiber_id.insertRow(row_number)
            self.table_skip_fiber_id.setItem(row_number, 0, QTableWidgetItem(str(79 + count)))

        return

    def add_skip_row(self):
        row_number = self.table_skip_fiber_id.rowCount()
        self.table_skip_fiber_id.insertRow(row_number)

        return

    def delete_skip_row(self):
        if self.table_skip_fiber_id.currentRow() is not None:
            self.table_skip_fiber_id.removeRow(self.table_skip_fiber_id.currentRow())

        return

    def read_skip_fiber_table(self):
        fiber_list = []
        for count in range(self.table_skip_fiber_id.rowCount()):
            tmp_id = int(self.table_skip_fiber_id.item(count, 0).text())
            if tmp_id > 0 and tmp_id <= 127:
                fiber_list.append(tmp_id)

        const_var.skip_fiber_list = sorted(fiber_list)

        return

    def updateFrameOverwrite(self):
        sender = self.sender()
        const_var.frameOverwrite = sender.text()

    def update_gui(self):
        self.read_skip_fiber_table()

        return
