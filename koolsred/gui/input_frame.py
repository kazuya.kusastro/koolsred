#!/usr/bin/python3
# coding: utf-8

### gui/input_frame.py: GUI for setting the input frame configrations

### import libraries
import datetime, glob, os, shutil

from PyQt5.QtWidgets import (QTableWidget, QTableWidgetItem, QLabel, QPushButton, QComboBox, QCheckBox, QFileDialog, QFrame, QGridLayout)
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

# KOOLS reduction modules
from koolsred import const_var

## define input box
class boxInput(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# define constants
        headerTable = ('File ID', 'Grism (or bias)', 'Flat', 'Comparison', 'Twilight', 'Standard star')
        self.grismChoices = ['No use', 'bias', 'None'] + const_var.grism_names
        self.lampChoices = ['--'] + const_var.comp_names

# initialize

# make widgets
#    self.buttonCopyObsFile = QPushButton('Copy obs. files')
#    self.buttonCopyObsFile.clicked.connect(self.copyObsFile)
#    self.buttonCopyObsFile.setFont(const_var.font)
#    self.buttonCopyObsFile.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.buttonImport = QPushButton('Import obs_log file')
        self.buttonImport.clicked.connect(self.import_obs_log)
        self.buttonImport.setFont(const_var.font)
        self.buttonImport.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.tableFrame = QTableWidget(0, 5)
        self.tableFrame.setHorizontalHeaderLabels(headerTable)
        self.tableFrame.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}')
        self.tableFrame.setColumnWidth(1, 120)

        self.buttonAddFrame = QPushButton('Add frame')
        self.buttonAddFrame.clicked.connect(self.addFrame)
        self.buttonAddFrame.setFont(const_var.font)
        self.buttonAddFrame.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.buttonDeleteFrame = QPushButton('Delete frame')
        self.buttonDeleteFrame.clicked.connect(self.deleteFrame)
        self.buttonDeleteFrame.setFont(const_var.font)
        self.buttonDeleteFrame.setStyleSheet('QWidget { background-color: #FFB0B0 }')

        self.buttonSortFrame = QPushButton('Sort frames')
        self.buttonSortFrame.clicked.connect(self.sortFrame)
        self.buttonSortFrame.setFont(const_var.font)
        self.buttonSortFrame.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.buttonDeleteAll = QPushButton('Delete all frames')
        self.buttonDeleteAll.clicked.connect(self.deleteAllFrames)
        self.buttonDeleteAll.setFont(const_var.font)
        self.buttonDeleteAll.setStyleSheet('QWidget { background-color: #FF0000 }')

#    self.labelLine = QLabel('--------------------')
#    self.labelLine.setFont(const_var.font)
#    self.labelLine.setAlignment(Qt.AlignCenter)
#
#    self.buttonUpdateList = QPushButton('Update list')
#    self.buttonUpdateList.clicked.connect(self.sendFrameList)
#    self.buttonUpdateList.setFont(const_var.font)
#    self.buttonUpdateList.setStyleSheet('QWidget { background-color: #B0FFB0 }')

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

#    layout.addWidget(self.buttonCopyObsFile, 0, 0)
        layout.addWidget(self.buttonImport, 0, 0, 1, 2)
        layout.addWidget(self.tableFrame, 1, 0, 1, 2)
        layout.addWidget(self.buttonAddFrame, 2, 0)
        layout.addWidget(self.buttonDeleteFrame, 2, 1)
        layout.addWidget(self.buttonSortFrame, 3, 0)
        layout.addWidget(self.buttonDeleteAll, 3, 1)

#    layout.addWidget(self.labelLine, 4, 0, 1, 2)
#
#    layout.addWidget(self.buttonUpdateList, 5, 0, 1, 2)

        self.setLayout(layout)

# function
    def import_obs_log(self):
        file_filter = 'Log files(*.log);; Text files(*.txt);; All files(*)'
        file_name = QFileDialog.getOpenFileName(self, 'Open file', './', file_filter)
#    if file_name[0]:
#      with open(file_name[0], 'r') as fin:
#      pass
#    else:
#      print('File open error.')
#      return 1

# input file
        if file_name[0] == '':
            return 0

        with open(file_name[0], 'r') as fin:
            lines = fin.readlines()

# import arguments
        dataInput = []
        for line in lines:
            ret = line.split()

            frame_id = ret[2]

            if float(ret[3]) == 0:
                grism = 'bias'
                frame_type = 'bias'
            else:
                grism = ret[7]

            if ret[4] in ['FLAT'] + const_var.comp_names:
                frame_type = ret[4]
            elif ret[4].lower() == 'twilight':
                frame_type = 'twilight'
            else:
                frame_type = 'Object'

            dataInput.append([frame_id, grism, frame_type])

# append list
        row_number = self.tableFrame.rowCount()
        for tmp_data in reversed(dataInput):
            self.tableFrame.insertRow(row_number)

            item = QTableWidgetItem(str(tmp_data[0]))
            self.tableFrame.setItem(row_number, 0, item)

            comboGrism = QComboBox()
            comboGrism.setFont(const_var.font)
            for tmp_name in self.grismChoices:
                comboGrism.addItem(tmp_name)
            comboGrism.setCurrentText(tmp_data[1])
            self.tableFrame.setCellWidget(row_number, 1, comboGrism)

            checkBox = QCheckBox()
            self.tableFrame.setCellWidget(row_number, 2, checkBox)
            if tmp_data[2] == 'FLAT':
                self.tableFrame.cellWidget(row_number, 2).setChecked(True)

            comboGrism = QComboBox()
            comboGrism.setFont(const_var.font)
            for tmp_name in ['--'] + const_var.comp_names:
                comboGrism.addItem(tmp_name)
            if tmp_data[2] in const_var.comp_names:
                comboGrism.setCurrentText(tmp_data[2])
            else:
                comboGrism.setCurrentIndex(0)
            self.tableFrame.setCellWidget(row_number, 3, comboGrism)

#      checkBox = QCheckBox()
#      self.tableFrame.setCellWidget(row_number, 3, checkBox)
#      if tmp_data[2] == 'Comparison':
#        self.tableFrame.cellWidget(row_number, 3).setChecked(True)

            checkBox = QCheckBox()
            self.tableFrame.setCellWidget(row_number, 4, checkBox)
            if tmp_data[2] == 'twilight':
                self.tableFrame.cellWidget(row_number, 4).setChecked(True)

# send frame lists
        self.sendFrameList()

    def addFrame(self):
        row_number = self.tableFrame.rowCount()
        self.tableFrame.insertRow(row_number)

        item = QTableWidgetItem()
        self.tableFrame.setItem(row_number, 0, item)

        comboGrism = QComboBox()
        comboGrism.setFont(const_var.font)
        for tmp_name in self.grismChoices:
            comboGrism.addItem(tmp_name)
        self.tableFrame.setCellWidget(row_number, 1, comboGrism)

        checkBox = QCheckBox()
        self.tableFrame.setCellWidget(row_number, 2, checkBox)

        comboLamp = QComboBox()
        comboLamp.setFont(const_var.font)
        for tmp_name in self.lampChoices:
            comboLamp.addItem(tmp_name)
        self.tableFrame.setCellWidget(row_number, 3, comboLamp)

        checkBox = QCheckBox()
        self.tableFrame.setCellWidget(row_number, 4, checkBox)

    def deleteFrame(self):
        if self.tableFrame.currentItem() is not None:
            self.tableFrame.removeRow(self.tableFrame.currentRow())

    def sortFrame(self):
        self.tableFrame.sortItems(0)

    def deleteAllFrames(self):
        reply = QMessageBox.question(self, 'Message', 'Are you sure to delete ALL frames?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.tableFrame.setRowCount(0)

    def sendFrameList(self):
        const_var.frameTable.clear()
        for count in range(self.tableFrame.rowCount()):
#      const_var.frameTable.append([self.tableFrame.item(count, 0).text(), self.tableFrame.cellWidget(count, 1).currentText(), self.tableFrame.cellWidget(count, 2).isChecked(), self.tableFrame.cellWidget(count, 3).isChecked()])
            tmp_list = [self.tableFrame.item(count, 0).text(), self.tableFrame.cellWidget(count, 1).currentText()]
            tmp_list.append(self.tableFrame.cellWidget(count, 2).isChecked())
            tmp_list.append(self.tableFrame.cellWidget(count, 3).currentText())
#      tmp_list.append(self.tableFrame.cellWidget(count, 3).isChecked())
            tmp_list.append(self.tableFrame.cellWidget(count, 4).isChecked())

            const_var.frameTable.append(tmp_list)

# clear frame tables
        const_var.frames = {'bias': [], 'no_grism': []}
        const_var.frames['flat'] = [[] for i in range(len(const_var.grism_names))]
        const_var.frames['Hg'] = [[] for i in range(len(const_var.grism_names))]
        const_var.frames['Ne'] = [[] for i in range(len(const_var.grism_names))]
        const_var.frames['Xe'] = [[] for i in range(len(const_var.grism_names))]
        const_var.frames['sstar'] = [[] for i in range(len(const_var.grism_names))]
        const_var.frames['twilight'] = [[] for i in range(len(const_var.grism_names))]
        const_var.frames['object'] = [[] for i in range(len(const_var.grism_names))]

        for tmp_list in const_var.frameTable:
# convert frame name
            frame_name = self.convertFrameFormat(tmp_list[0])
# devide list
            if tmp_list[1] == 'bias':
                const_var.frames['bias'].append(frame_name)
                continue

            elif tmp_list[1] == 'None':
                const_var.frames['no_grism'].append(frame_name)
                continue

            elif tmp_list[1] in const_var.grism_names:
                grism_id = const_var.grism_names.index(tmp_list[1])
            else:
                continue

            if tmp_list[2]:
                const_var.frames['flat'][grism_id].append(frame_name)

            if tmp_list[3] in const_var.comp_names:
                tmp_comp = tmp_list[3]
                const_var.frames[tmp_comp][grism_id].append(frame_name)

            if tmp_list[4]:
                const_var.frames['twilight'][grism_id].append(frame_name)

            const_var.frames['object'][grism_id].append(frame_name)

    def convertFrameFormat(self, frame_in):
        if const_var.frameFormat == 'kls%08d.fits':
            return 'kls{}.fits'.format(frame_in.zfill(8))
        elif const_var.frameFormat == 'kif%08d.fits':
            return 'kif{}.fits'.format(frame_in.zfill(8))
        else:
            return frame_in

    def updateGUI(self):
        self.sortFrame()
        self.sendFrameList()
