#!/usr/bin/python3
# coding: utf-8

### gui/quick_image.py: GUI for reconstructing 2D images quickly from frames with a grism

### import libraries
from astropy.io import fits
import numpy as np
import math
import os
from scipy import (integrate, interpolate, signal)
import sys
import time

import PyQt5
from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow, QTabWidget, QTableWidget, QTableWidgetItem, QLabel, QLineEdit, QTextEdit, QPushButton, QRadioButton, QButtonGroup, QComboBox, QCheckBox, QMessageBox, QFileDialog, QFrame, QVBoxLayout, QGridLayout)
from PyQt5.QtGui import QFont
from PyQt5.QtCore import (Qt, QTimer)

import pyqtgraph as pg
from pyqtgraph.Qt import (QtCore, QtGui)

# KOOLS reduction modules
from koolsred import (const_var, common_func, data_red)
from koolsred.gui import custom_window # for pyqtgraph

### set constants
n_fiber = 127
combine_x_width = 10
combine_y_width = 5

output_name = 'tmp-2d-image.fits'

#fiber_id_table = ((0, 0, 0, 0, 0, 0, 54, 0, 46, 0, 80, 0, 61, 0, 50, 0, 55, 0, 43, 0, 0 ,0, 0, 0, 0),
#  (0, 0, 0, 0, 0, 49, 0, 118, 0, 22, 0, 53, 0, 45, 0, 44, 0, 76, 0, 66, 0, 0, 0, 0, 0),
#  (0, 0, 0, 0, 48, 0, 94, 0, 100, 0, 6, 0, 78, 0, 81, 0, 60, 0, 52, 0, 67, 0, 0, 0, 0),
#  (0, 0, 0, 110, 0, 112, 0, 19, 0, 105, 0, 125, 0, 74, 0, 58, 0, 77, 0, 56, 0, 47, 0, 0, 0),
#  (0, 0, 123, 0, 107, 0, 18, 0, 86, 0, 13, 0, 70, 0, 84, 0, 85, 0, 51, 0, 57, 0, 79, 0, 0),
#  (0, 96, 0, 117, 0, 108, 0, 9, 0, 91, 0, 120, 0, 27, 0, 71, 0, 68, 0, 116, 0, 82, 0, 72, 0),
#  (99, 0, 97, 0, 98, 0, 92, 0, 23, 0, 12, 0, 37, 0, 39, 0, 26, 0, 63, 0, 83, 0, 73, 0, 59),
#  (0, 127, 0, 114, 0, 124, 0, 111, 0, 30, 0, 34, 0, 122, 0, 35, 0, 3, 0, 64, 0, 75, 0, 65, 0),
#  (0, 0, 109, 0, 113, 0, 87, 0, 88, 0, 21, 0, 121, 0, 11, 0, 32, 0, 14, 0, 95, 0, 69, 0, 0),
#  (0, 0, 0, 102, 0, 93, 0, 5, 0, 126, 0, 8, 0, 36, 0, 25, 0, 20, 0, 106, 0, 2, 0, 0, 0),
#  (0, 0, 0, 0, 89, 0, 38, 0, 119, 0, 10, 0, 24, 0, 17, 0, 41, 0, 33, 0, 62, 0, 0, 0, 0),
#  (0, 0, 0, 0, 0, 103, 0, 115, 0, 90, 0, 101, 0, 7, 0, 1, 0, 42, 0, 40, 0, 0, 0, 0, 0),
#  (0, 0, 0, 0, 0, 0, 104, 0, 29, 0, 16, 0, 31, 0, 15, 0, 28, 0, 4, 0, 0, 0, 0, 0, 0))

fiber_id_table = (
  (0, 0, 0, 0, 0, 0, 104, 0, 29, 0, 16, 0, 31, 0, 15, 0, 28, 0, 4, 0, 0, 0, 0, 0, 0),
  (0, 0, 0, 0, 0, 103, 0, 115, 0, 90, 0, 101, 0, 7, 0, 1, 0, 42, 0, 40, 0, 0, 0, 0, 0),
  (0, 0, 0, 0, 89, 0, 38, 0, 119, 0, 10, 0, 24, 0, 17, 0, 41, 0, 33, 0, 62, 0, 0, 0, 0),
  (0, 0, 0, 102, 0, 93, 0, 5, 0, 126, 0, 8, 0, 36, 0, 25, 0, 20, 0, 106, 0, 2, 0, 0, 0),
  (0, 0, 109, 0, 113, 0, 87, 0, 88, 0, 21, 0, 121, 0, 11, 0, 32, 0, 14, 0, 95, 0, 69, 0, 0),
  (0, 127, 0, 114, 0, 124, 0, 111, 0, 30, 0, 34, 0, 122, 0, 35, 0, 3, 0, 64, 0, 75, 0, 65, 0),
  (99, 0, 97, 0, 98, 0, 92, 0, 23, 0, 12, 0, 37, 0, 39, 0, 26, 0, 63, 0, 83, 0, 73, 0, 59),
  (0, 96, 0, 117, 0, 108, 0, 9, 0, 91, 0, 120, 0, 27, 0, 71, 0, 68, 0, 116, 0, 82, 0, 72, 0),
  (0, 0, 123, 0, 107, 0, 18, 0, 86, 0, 13, 0, 70, 0, 84, 0, 85, 0, 51, 0, 57, 0, 79, 0, 0),
  (0, 0, 0, 110, 0, 112, 0, 19, 0, 105, 0, 125, 0, 74, 0, 58, 0, 77, 0, 56, 0, 47, 0, 0, 0),
  (0, 0, 0, 0, 48, 0, 94, 0, 100, 0, 6, 0, 78, 0, 81, 0, 60, 0, 52, 0, 67, 0, 0, 0, 0),
  (0, 0, 0, 0, 0, 49, 0, 118, 0, 22, 0, 53, 0, 45, 0, 44, 0, 76, 0, 66, 0, 0, 0, 0, 0),
  (0, 0, 0, 0, 0, 0, 54, 0, 46, 0, 80, 0, 61, 0, 50, 0, 55, 0, 43, 0, 0 ,0, 0, 0, 0)
)

fiber_pos_x = (0,
  16, 22, 18, 19,  8, 11, 14, 12,  8, 11,
  15, 11, 11, 19, 15, 11, 15,  7,  8, 18,
  11, 10,  9, 13, 16, 17, 14, 17,  9, 10,
  13, 17, 19, 12, 16, 14, 13,  7, 15, 20,
  17, 18, 19, 16, 14,  9, 22,  5,  6, 15,
  19, 19, 12,  7, 17, 20, 21, 16, 25, 17,
  13, 21, 19, 20, 24, 20, 21, 18, 23, 13,
  16, 24, 23, 14, 22, 18, 18, 13, 23, 11,
  15, 22, 21, 15, 17,  9,  7,  9,  5, 10,
  10,  7,  6,  7, 21,  2,  3,  5,  1,  9,
  12,  4,  6,  7, 10, 20,  5,  6,  3,  4,
   8,  6,  5,  4,  8, 20,  4,  8,  9, 12,
  13, 14,  3,  6, 12, 10,  2)
#fiber_pos_y = (0,
#  23, 19, 15, 25, 19,  5, 23, 19, 11, 21,
#  17, 13,  9, 17, 25, 25, 21,  9,  7, 19,
#  17,  3, 13, 21, 19, 13, 11, 25, 25, 15,
#  25, 17, 21, 15, 15, 19, 13, 21, 13, 23,
#  21, 23,  1,  3,  3,  1,  7,  5,  3,  1,
#   9,  5,  3,  1,  1,  7,  9,  7, 13,  5,
#   1, 21, 13, 15, 15,  3,  5, 11, 17,  9,
#  11, 11, 13,  7, 15,  3,  7,  5,  9,  1,
#   5, 11, 13,  9,  9,  9, 17, 17, 21, 23,
#  11, 13, 19,  5, 17, 11, 13, 13, 13,  5,
#  23, 19, 23, 25,  7, 19,  9, 11, 17,  7,
#  15,  7, 17, 15, 23, 11, 11,  3, 21, 11,
#  17, 15,  9, 15,  7, 19, 15)
fiber_pos_y = (0,
   3,  7, 11,  1,  7, 21,  3,  7, 15,  5,
   9, 13, 17,  9,  1,  1,  5, 17, 19,  7,
   9, 23, 13,  5,  7, 13, 15,  1,  1, 11,
   1,  9,  5, 11, 11,  7, 13,  5, 13,  3,
   5,  3, 25, 23, 23, 25, 19, 21, 23, 25,
  17, 21, 23, 25, 25, 19, 17, 19, 13, 21,
  25,  5, 13, 11, 11, 23, 21, 15,  9, 17,
  15, 15, 13, 19, 11, 23, 19, 21, 17, 25,
  21, 15, 13, 17, 17, 17,  9,  9,  5,  3,
  15, 13,  7, 21,  9, 15, 13, 13, 13, 21,
   3,  7,  3,  1, 19,  7, 17, 15,  9, 19,
  11, 19,  9, 11,  3, 15, 15, 23,  5, 15,
   9, 11, 17, 11, 19,  7, 11)


### define functions
#def reductionSequence():
#  if commonSetting.autoReduction == 'Off':
#    return 0
#
#  updateFrameList()

### define common information struct
#class commonSetting:
## define font
#  fontLabel = QFont()
#  fontLabel.setPixelSize(28)
#  font = QFont()
#  font.setPixelSize(20)
#  fontToolTip = QFont()
#  fontToolTip.setPixelSize(16)

# common variables

## define directory box
class boxQuickImage(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize

# make widgets
        miniLabelGrism = QLabel('Grism')
        miniLabelGrism.setFont(const_var.font)
        self.comboGrism = QComboBox()
        self.comboGrism.setFont(const_var.font)
        self.comboGrism.currentIndexChanged.connect(self.updateComboFrames)

        miniLabelRefFrame = QLabel('Reference frame')
        miniLabelRefFrame.setFont(const_var.font)
        self.comboRefFrame = QComboBox()
        self.comboRefFrame.setFont(const_var.font)

        miniLabelObjFrame = QLabel('Object frame')
        miniLabelObjFrame.setFont(const_var.font)
        self.comboObjFrame = QComboBox()
        self.comboObjFrame.setFont(const_var.font)

        self.buttonOpenFrame = QPushButton('Open object frame')
        self.buttonOpenFrame.setFont(const_var.font)
        self.buttonOpenFrame.setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.buttonOpenFrame.clicked.connect(self.openFrame)

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(miniLabelGrism, 0, 0)
        layout.addWidget(self.comboGrism, 0, 1)
        layout.addWidget(miniLabelRefFrame, 1, 0)
        layout.addWidget(self.comboRefFrame, 1, 1)
        layout.addWidget(miniLabelObjFrame, 2, 0)
        layout.addWidget(self.comboObjFrame, 2, 1)
        layout.addWidget(self.buttonOpenFrame, 3, 0, 1, 2)

        self.setLayout(layout)

## functions
    def openFrame(self):
# set the object frame
        self.ref_frame = self.comboRefFrame.currentText()
        self.obj_frame = self.comboObjFrame.currentText()

        if not os.path.isfile(self.obj_frame):
            print('Cannot open the input frame. {} does not exist.'.format(self.obj_frame))
            return 1

# PyQtGraph
        pg.setConfigOptions(imageAxisOrder='row-major')

        pg.mkQApp()
#    self.win = pg.GraphicsLayoutWidget()
        self.win = custom_window.CustomGraphicsWindow()
        self.win.setWindowTitle('Make quick image: {}'.format(self.obj_frame))

# A plot area (ViewBox + axes) for displaying the image
        self.p1 = self.win.addPlot()
        self.p1.setAspectLocked(True)

# Item for displaying image data
        self.img = pg.ImageItem()
        self.p1.addItem(self.img)

# Custom ROI for selecting an image region
        self.roi = pg.ROI([1000, 200], [10, 1300])
        self.roi.addScaleHandle([0.5, 1], [0.5, 0.5])
        self.roi.addScaleHandle([0, 0.5], [0.5, 0.5])
        self.p1.addItem(self.roi)
        self.roi.setZValue(10)  # make sure ROI is drawn above image

# Contrast/color control
        hist = pg.HistogramLUTItem()
        hist.setImageItem(self.img)
        self.win.addItem(hist)

        self.win.resize(800, 600)
        self.win.show()

# Another plot area for displaying ROI data
#    self.win.nextRow()
#    self.p2 = self.win.addPlot(colspan=2)
#    p2.setMaximumWidth(250)
#    self.p2.setMaximumHeight(300)
#    p2.rotate(330)

# Generate image data
        self.data = fits.getdata(self.obj_frame, view = np.ndarray)
        self.img.setImage(self.data)
        hist.setLevels(self.data.min(), self.data.max())

# set position and scale of image
#    self.img.scale(1, 1)
#    self.img.translate(-500, 0)

# zoom to fit image
#    p1.autoRange()

# Callbacks for handling user interaction
#    self.updatePlotExtract()
#    self.roi.sigRegionChanged.connect(self.updatePlotExtract)

#  def updatePlotExtract(self):
#    selected = self.roi.getArrayRegion(self.object_data, self.img)
#    self.p2.plot(selected.mean(axis=1), clear=True)

        def keyPressed(event):
            if event.key() == Qt.Key_F:
                self.makeQuickImage()
            elif event.key() == Qt.Key_Question:
                self.showHelp()

        self.win.sigKeyPress.connect(keyPressed)

    def showHelp(self):
        print('''
      Help for making a quick image.
        f -- find peaks and make a quick image at the region.
        ? -- show this help.
        ''')

    def makeQuickImage(self):
# check weather the graph window appears
        if not hasattr(self, 'p1'):
            return 0

# cut ROI region
#    selected = self.roi.getArrayRegion(self.ref_data, self.img)
#    pos_roi, size_roi = self.roi.pos(), self.roi.size()

        data_red.quick_image.makeImage(self.obj_frame, self.ref_frame, self.roi.pos(), self.roi.size())

    def findPeaks(self):
        self.findPeak_Y()
        self.combineObjectFlux()
        self.make2DImage()

    def findPeak_Y(self):
# cut ROI region
        selected = self.roi.getArrayRegion(self.ref_data, self.img)
        pos_roi, size_roi = self.roi.pos(), self.roi.size()

# find peak and exclude fainter ones
        mean = selected.mean(axis=1)
        peak_y = signal.argrelmax(mean, order=2)
        peak_list = [(tmp_y, mean[tmp_y]) for tmp_y in peak_y[0]]
        peak_array = np.array(peak_list)
        sort_array = peak_array[np.argsort(peak_array[:, 1])[::-1]]
        sort_array2 = sort_array[np.argsort(sort_array[:n_fiber, 0])]
#    print(sort_array2[:, 0])

# reload image
        self.reload_p1()

# mark peak positions on p1
        pen = pg.mkPen(color='g', width=5)

        plot_x = [pos_roi[0], pos_roi[0] + combine_x_width]
#    self.init_x = plot_x[0] + combine_x_width / 2

        self.init_y_list = []
        for tmp_y in sort_array2[:, 0]:
            center_y = self.grav_center(pos_roi[0], pos_roi[1] + tmp_y, size_roi[0], combine_y_width)
            self.init_y_list.append(center_y - pos_roi[1])

            plot_y = [center_y + 0.5] * 2
#      print('peak position = {} {}'.format(plot_x, plot_y))
            self.p1.addItem(pg.PlotDataItem(x=plot_x, y=plot_y, pen=pen))

    def grav_center(self, pos_x, pos_y, width, height):
        height_half = int((height - 1) / 2)
        start_x, end_x = int(pos_x), int(pos_x) + int(width)
        start_y, end_y = int(pos_y) - height_half, int(pos_y) + height_half + 1
        cut_array = self.ref_data[start_y:end_y, start_x:end_x]

        count_sum = np.sum(cut_array)
        count_sum_x = np.sum(cut_array, axis=1)
        pos_y_array = [i - height_half for i in range(height)]
        pos_count_sum = np.sum(count_sum_x * pos_y_array)

        return pos_count_sum / count_sum + pos_y

    def reload_p1(self):
        self.p1.clear()
        self.p1.addItem(self.img)
        self.p1.addItem(self.roi)

    def combineObjectFlux(self):
        flux_y_array = self.roi.getArrayRegion(self.object_data, self.img)
        flux_y = flux_y_array.sum(axis=1)

#    print(flux_y)

        flux_x = range(len(flux_y))
        func_flux = interpolate.interp1d(flux_x, flux_y)
#    self.flux_list = [integrate.quad(func_flux, tmp_x - combine_y_width * 0.5, tmp_x + combine_y_width * 0.5) for tmp_x in self.init_y_list]

        self.flux_list = []
        for tmp_x in self.init_y_list:
            tmp_flux = 0

            min_x, max_x = tmp_x - combine_y_width * 0.5, tmp_x + combine_y_width * 0.5
            min_int, max_int = math.floor(min_x), math.floor(max_x)
            min_float, max_float = min_x - min_int, max_x - max_int

            tmp_flux += (func_flux(min_x) + func_flux(min_int + 1)) * (1 - min_float) * 0.5
            for count in range(min_int, max_int):
                tmp_flux += (func_flux(count) + func_flux(count + 1)) * 0.5
            tmp_flux += (func_flux(max_int) + func_flux(max_x)) * max_float * 0.5

            self.flux_list.append(tmp_flux)

#    print(self.flux_list)

        return 0

    def make2DImage(self):
        image_size = 25
        output_data = np.zeros((image_size, image_size))

        for count in range(n_fiber):
            output_data[fiber_pos_y[count + 1] - 1][fiber_pos_x[count + 1] - 1] = self.flux_list[count]

#    for count2 in range(int((image_size + 1) / 2)):
#      for count1 in range(image_size):
#        tmp_id = fiber_id_table[count2][count1]
#        if tmp_id != 0:
#          output_data[count2 * 2][count1] = self.flux_list[tmp_id - 1]

        fits.writeto(output_name, output_data, overwrite = True)

        return 0

    def updateComboFrames(self):
        self.comboRefFrame.clear()
        self.comboObjFrame.clear()

        grism = self.comboGrism.currentText()
        if grism == '':
            return 0
        grism_id = const_var.grism_names.index(grism)

        flat_comb = 'flat-{}-comb.fits'.format(grism.lower())
        if os.path.isfile(flat_comb):
            self.comboRefFrame.addItem(flat_comb)

        if len(const_var.frames['object'][grism_id]) > 0:
            for tmp_frame in const_var.frames['object'][grism_id]:
                tmp_frame_suffix = common_func.add_suffix([tmp_frame], '-gain')[0]
                self.comboRefFrame.addItem(tmp_frame_suffix)
                self.comboObjFrame.addItem(tmp_frame_suffix)

    def updateGUI(self):
        self.comboGrism.clear()
        for count, tmp_grism in enumerate(const_var.grism_names):
            if len(const_var.frames['object'][count]) > 0:
                self.comboGrism.addItem(tmp_grism)
