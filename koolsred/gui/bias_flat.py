#!/usr/bin/python3
# coding: utf-8

### gui/bias_flat.py: GUI for subtracting and cutting overscan regions, subtracting bias, and combining flat frames of each grism

### import libraries
import multiprocessing
import os

from PyQt5.QtWidgets import (QLabel, QPushButton, QRadioButton, QButtonGroup, QFrame, QGridLayout)
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

# KOOLS reduction modules
from koolsred import (const_var, common_func, data_red)

## define combining flat box
class boxBiasFlat(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
#    self.buttonCombine = {}

# make buttons and input/output boxes
        self.buttonOverscanBias = QPushButton('Execute overscan and bias processes')
        self.buttonOverscanBias.setFont(const_var.font)
        self.buttonOverscanBias.setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.buttonOverscanBias.clicked.connect(self.execOverscanBias)

        self.button_remove_cosmic = QPushButton('Remove cosmic rays')
        self.button_remove_cosmic.setFont(const_var.font)
        self.button_remove_cosmic.setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.button_remove_cosmic.clicked.connect(self.remove_cosmic)

        miniLabelModeFlat = QLabel('Combine mode (Flat)')
        miniLabelModeFlat.setFont(const_var.font)
        radioFlatMedian = QRadioButton('Median')
        radioFlatMedian.setFont(const_var.font)
        radioFlatMedian.setChecked(True)
        radioFlatMean = QRadioButton('Mean')
        radioFlatMean.setFont(const_var.font)
        radioFlatMean.setChecked(False)
        self.groupModeFlat = QButtonGroup()
        self.groupModeFlat.addButton(radioFlatMedian, 1)
        self.groupModeFlat.addButton(radioFlatMean, 2)

        miniLabelModeComp = QLabel('Combine mode (Comp)')
        miniLabelModeComp.setFont(const_var.font)
        radioCompMedian = QRadioButton('Median')
        radioCompMedian.setFont(const_var.font)
        radioCompMedian.setChecked(False)
        radioCompMean = QRadioButton('Mean')
        radioCompMean.setFont(const_var.font)
        radioCompMean.setChecked(True)
        self.groupModeComp = QButtonGroup()
        self.groupModeComp.addButton(radioCompMedian, 1)
        self.groupModeComp.addButton(radioCompMean, 2)

        self.buttonCombFlatCompGain = QPushButton('Combine flat and comparison frames and adjust gain')
        self.buttonCombFlatCompGain.clicked.connect(self.combineFlatCompGain)
        self.buttonCombFlatCompGain.setFont(const_var.font)
        self.buttonCombFlatCompGain.setStyleSheet('QWidget { background-color: #B0FFB0 }')

#    self.buttonCombineAll = QPushButton('Combine flat frames for all grisms')
#    self.buttonCombineAll.clicked.connect(self.combineFlatAll)
#    self.buttonCombineAll.setFont(const_var.font)
#    self.buttonCombineAll.setStyleSheet('QWidget { background-color: #B0FFB0 }')
#
#    for tmp_grism in const_var.grism_names:
#      self.buttonCombine[tmp_grism] = QPushButton(tmp_grism)
#      self.buttonCombine[tmp_grism].clicked.connect(self.combineFlatOne)
#      self.buttonCombine[tmp_grism].setFont(const_var.font)
#      self.buttonCombine[tmp_grism].setStyleSheet('QWidget { background-color: #B0FFB0 }')
#
#    self.buttonAdjustGain = QPushButton('Adjust gain of each channel')
#    self.buttonAdjustGain.clicked.connect(self.adjustGain)
#    self.buttonAdjustGain.setFont(const_var.font)
#    self.buttonAdjustGain.setStyleSheet('QWidget { background-color: #B0FFB0 }')

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(self.buttonOverscanBias, 0, 0, 1, 2)
        layout.addWidget(self.button_remove_cosmic, 0, 2)
        layout.addWidget(miniLabelModeFlat, 1, 0)
        layout.addWidget(radioFlatMedian, 1, 1)
        layout.addWidget(radioFlatMean, 1, 2)
        layout.addWidget(miniLabelModeComp, 2, 0)
        layout.addWidget(radioCompMedian, 2, 1)
        layout.addWidget(radioCompMean, 2, 2)

        layout.addWidget(self.buttonCombFlatCompGain, 3, 0, 1, 3)
#    layout.addWidget(self.buttonCombineAll, 1, 0, 1, 4)
#    for count, tmp_grism in enumerate(const_var.grism_names):
#      layout.addWidget(self.buttonCombine[tmp_grism], 2, count)
#    layout.addWidget(self.buttonAdjustGain, 3, 0, 1, 4)

        self.setLayout(layout)

# function
    def execOverscanBias(self):
        self.subCutOverscan()
        self.combineBias()
        self.subtractBias()

    def subCutOverscan(self):
        print('Process of cutting overscan regions starts.')

# make input list
        input_list = const_var.frames['bias'][:] + const_var.frames['no_grism'][:]
        for grism_id in range(len(const_var.grism_names)):
            input_list += const_var.frames['object'][grism_id]

        input_list_uniq = sorted(list(set(input_list)))

# make output list
        output_list = common_func.add_suffix(input_list_uniq, '-cut')

# fork process
        num_cpu = int(const_var.cpuThreadLimit)
        input_list_div = common_func.divide_frame_list(input_list_uniq, num_cpu)

        def reduction(in_list):
            output_list = common_func.add_suffix(in_list, '-cut')
            for input_fits, output_fits in zip(in_list, output_list):
                if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                    data_red.bias_flat.subtract_cut_overscan(input_fits, output_fits)
#          print('{} --> {}.'.format(input_fits, output_fits))

        jobs = []
        for tmp_input in input_list_div:
            job = multiprocessing.Process(target=reduction, args=(tmp_input,))
            jobs.append(job)
            job.start()

        [job.join() for job in jobs]

        print('Process of cutting overscan regions finished.')

    def combineBias(self):
        print('Process of combining bias frames starts.')

# make lib directory
        if not os.path.isdir('lib'):
            os.mkdir('lib')

        if const_var.frameOverwrite == 'Yes' or not os.path.isfile(const_var.bias_comb):
# make input list
            input_list = common_func.add_suffix(const_var.frames['bias'], '-cut')
            input_list_uniq = sorted(list(set(input_list)))

            if len(input_list) > 0:
                input_list_str = common_func.convert_list_to_str(input_list)
#                print('{} --> {}'.format(input_list_str, const_var.bias_comb))
                data_red.bias_flat.combine_frames(input_list_str, const_var.bias_comb, 'median')

        print('Process of combining bias frames finished.')

    def subtractBias(self):
        print('Process of bias subtraction starts.')
# make input list
        input_list = const_var.frames['no_grism'][:]
        for grism_id in range(len(const_var.grism_names)):
            input_list += const_var.frames['object'][grism_id]

        input_list_uniq = sorted(list(set(input_list)))
        input_list_suffix = common_func.add_suffix(input_list_uniq, '-cut')
        output_list_suffix = common_func.add_suffix(input_list_uniq, '-bias')

# combine bias frames
        for input_fits, output_fits in zip(input_list_suffix, output_list_suffix):
            if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                data_red.data_func.arith_frame(input_fits, 'sub', const_var.bias_comb, output_fits)
                print('{} --> {}'.format(input_fits, output_fits))

        print('Process of bias subtraction finished.')

    def remove_cosmic(self):
        print('Process of removing cosmic rays starts.')
# make input list
        input_list = const_var.frames['no_grism'][:]
        for grism_id in range(len(const_var.grism_names)):
            input_list += const_var.frames['object'][grism_id]

        input_list_uniq = sorted(list(set(input_list)))
        input_list_suffix = common_func.add_suffix(input_list_uniq, '-bias')
        output_list_suffix = common_func.add_suffix(input_list_uniq, '-bias')

# remove cosmic rays
        for input_fits, output_fits in zip(input_list_suffix, output_list_suffix):
            if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                data_red.bias_flat.remove_cosmic(input_fits, output_fits)
#                print('{} --> {}'.format(input_fits, output_fits))

        print('Process of removing cosmic rays finished.')

    def combineFlatCompGain(self):
        self.combineFlatAll()
        self.combineCompAll()
        self.adjustGain()

    def combineFlatAll(self):
        print('Process of combining flat frames starts.')

        for grism_id, grism_name in enumerate(const_var.grism_names):
#      grism_name = const_var.grism_names[grism_id]
            mode = self.groupModeFlat.checkedButton().text().lower()

            input_list = const_var.frames['flat'][grism_id][:]
            input_list_uniq = sorted(list(set(input_list)))
            if len(input_list_uniq) < 1:
                print('No flat frame for {}.'.format(grism_name))
                continue
#        return 0

            input_list_suffix = common_func.add_suffix(input_list_uniq, '-bias')
            input_list_str = common_func.convert_list_to_str(input_list_suffix)
            output_fits = 'flat-{}-comb.fits'.format(grism_name.lower())

            if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                print('{} --> {} combine with {}'.format(input_list_str, output_fits, mode))
                data_red.bias_flat.combine_frames(input_list_str, output_fits, mode)

        print('Process of combining flat frames finished.')

#  def combineFlatOne(self):
#    button = self.sender()
#    for count, tmp_grism in enumerate(const_var.grism_names):
#      if button is self.buttonCombine[tmp_grism]:
#        grism_id = count
#        break
#    else:
#      print('Input is invalid.')
#      return 1
#
#    self.combineFlat(grism_id)

    def combineCompAll(self):
        print('Process of combining comparison frames starts.')

        for grism_id, grism_name in enumerate(const_var.grism_names):
#      grism_name = const_var.grism_names[grism_id]
            mode = self.groupModeComp.checkedButton().text().lower()

            for tmp_lamp in const_var.comp_names:
                input_list = const_var.frames[tmp_lamp][grism_id][:]
                input_list_uniq = sorted(list(set(input_list)))
                if len(input_list_uniq) < 1:
                    print('No {} frame for {}.'.format(tmp_lamp, grism_name))
                    continue

                input_list_suffix = common_func.add_suffix(input_list_uniq, '-bias')
                input_list_str = common_func.convert_list_to_str(input_list_suffix)
                output_fits = '{}-{}-comb.fits'.format(tmp_lamp, grism_name.lower())

                if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                    print('{} --> {} combine with {}'.format(input_list_str, output_fits, mode))
                    data_red.bias_flat.combine_frames(input_list_str, output_fits, mode)

        print('Process of combining comparison frames finished.')

    def adjustGain(self):
        print('Process of gain adjustment starts.')
# make input list
        input_list = []
        for grism_id in range(len(const_var.grism_names)):
            input_list += const_var.frames['object'][grism_id]

        input_list_uniq = sorted(list(set(input_list)))
        input_list_suffix = common_func.add_suffix(input_list_uniq, '-bias')
        output_list_suffix = common_func.add_suffix(input_list_uniq, '-gain')

# add combined flat frames if exist
        gain_input = ''

        for tmp_grism in const_var.grism_names:
            tmp_flat = 'flat-{}-comb.fits'.format(tmp_grism.lower())
            if os.path.isfile(tmp_flat):
                gain_input = tmp_flat
                input_list_suffix.append(tmp_flat)
                output_list_suffix.append(tmp_flat.replace('-comb.fits', '-gain.fits'))

# add combined comparison frames if exist
        for tmp_grism in const_var.grism_names:
            for tmp_lamp in const_var.comp_names:
                tmp_comp = '{}-{}-comb.fits'.format(tmp_lamp, tmp_grism.lower())
                if os.path.isfile(tmp_comp):
                    input_list_suffix.append(tmp_comp)
                    output_list_suffix.append(tmp_comp.replace('-comb.fits', '-gain.fits'))

# calculate gain ratios
        gain_file = 'lib/ccd-gain.dat'

        if gain_input != '':
            gain_list = data_red.bias_flat.calc_gain_ratio(gain_input)

# export the gain list
            if not os.path.isdir('lib'):
                os.mkdir('lib')

            gain_file = 'lib/ccd-gain.dat'
            if not os.path.isfile(gain_file):
                with open(gain_file, 'w') as fout:
                    tmp_output = ''
                    for tmp_gain in gain_list:
                        tmp_output += str(tmp_gain) + ' '
                    fout.write(tmp_output[:-1])

                print('Export {}.'.format(gain_file))

# gain adjustment for frames
        for input_fits, output_fits in zip(input_list_suffix, output_list_suffix):
            if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                data_red.bias_flat.adjust_gain(input_fits, output_fits, gain_file)
#        print('{} --> {}'.format(input_fits, output_fits))

        print('Process of gain adjustment finished.')
