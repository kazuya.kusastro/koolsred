#!/usr/bin/python3
# coding: utf-8

### gui/overscan_bias.py: GUI for cutting overscan regions, combining bias frames, and subtracting the bias frame

### import libraries
import multiprocessing
import os

from PyQt5.QtWidgets import (QLabel, QPushButton, QFrame, QGridLayout)
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

# KOOLS reduction modules
from koolsred import (const_var, common_func, data_red)

# define overscan and bias box
class boxOverscanBias(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize

# make buttons and input/output boxes
        self.buttonOverscanBias = QPushButton('Execute overscan and bias processes')
        self.buttonOverscanBias.setFont(const_var.font)
        self.buttonOverscanBias.setStyleSheet('QWidget { background-color: #B0FFB0 }')
        self.buttonOverscanBias.clicked.connect(self.execOverscanBias)

#    self.buttonSubCut = QPushButton('Subtract and cut overscan regions')
#    self.buttonSubCut.clicked.connect(self.subCutOverscan)
#    self.buttonSubCut.setFont(const_var.font)
#    self.buttonSubCut.setStyleSheet('QWidget { background-color: #B0FFB0 }')
#
#    self.buttonCombBias = QPushButton('Combine bias frames')
#    self.buttonCombBias.clicked.connect(self.combineBias)
#    self.buttonCombBias.setFont(const_var.font)
#    self.buttonCombBias.setStyleSheet('QWidget { background-color: #B0FFB0 }')
#
#    self.buttonSubBias = QPushButton('Subtract bias')
#    self.buttonSubBias.clicked.connect(self.subtractBias)
#    self.buttonSubBias.setFont(const_var.font)
#    self.buttonSubBias.setStyleSheet('QWidget { background-color: #B0FFB0 }')

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(self.buttonOverscanBias, 0, 0)

#    layout.addWidget(self.buttonSubCut, 0, 0)
#    layout.addWidget(self.buttonCombBias, 1, 0)
#    layout.addWidget(self.buttonSubBias, 2, 0)

        self.setLayout(layout)

# function
    def execOverscanBias(self):
        self.subCutOverscan()
        self.combineBias()
        self.subtractBias()

    def subCutOverscan(self):
        print('Process of cutting overscan regions starts.')

# make input list
        input_list = const_var.frames['bias'][:] + const_var.frames['no_grism'][:]
        for grism_id in range(len(const_var.grism_names)):
            input_list += const_var.frames['object'][grism_id]

        input_list_uniq = sorted(list(set(input_list)))

# make output list
        output_list = common_func.add_suffix(input_list_uniq, '-cut')

# fork process
        num_cpu = int(const_var.cpuThreadLimit)
        input_list_div = common_func.divide_frame_list(input_list_uniq, num_cpu)

        def reduction(in_list):
            output_list = common_func.add_suffix(in_list, '-cut')
            for input_fits, output_fits in zip(in_list, output_list):
                if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                    data_red.bias_flat.subtract_cut_overscan(input_fits, output_fits)
#          print('{} --> {}.'.format(input_fits, output_fits))

        jobs = []
        for tmp_input in input_list_div:
            job = multiprocessing.Process(target=reduction, args=(tmp_input,))
            jobs.append(job)
            job.start()

        [job.join() for job in jobs]

        print('Process of cutting overscan regions finished.')

    def combineBias(self):
        print('Process of combining bias frames starts.')
        if const_var.frameOverwrite == 'Yes' or not os.path.isfile(const_var.bias_comb):
# make input list
            input_list = common_func.add_suffix(const_var.frames['bias'], '-cut')
            input_list_uniq = sorted(list(set(input_list)))

            input_list_str = common_func.convert_list_to_str(input_list)
            print('{} --> {}'.format(input_list_str, const_var.bias_comb))
            data_red.bias_flat.combine_frames(input_list_str, const_var.bias_comb, 'median')

        print('Process of combining bias frames finished.')

    def subtractBias(self):
        print('Process of bias subtraction starts.')
# make input list
        input_list = const_var.frames['no_grism'][:]
        for grism_id in range(len(const_var.grism_names)):
            input_list += const_var.frames['object'][grism_id]

        input_list_uniq = sorted(list(set(input_list)))
        input_list_suffix = common_func.add_suffix(input_list_uniq, '-cut')
        output_list_suffix = common_func.add_suffix(input_list_uniq, '-bias')

# combine bias frames
        for input_fits, output_fits in zip(input_list_suffix, output_list_suffix):
            if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                data_red.bias_flat.arith_frame(input_fits, 'sub', const_var.bias_comb, output_fits)
                print('{} --> {}'.format(input_fits, output_fits))

        print('Process of bias subtraction finished.')
