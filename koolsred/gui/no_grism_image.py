#!/usr/bin/python3
# coding: utf-8

### gui/no_grism_image.py: GUI for reconstructing 2D images from frames without a grism

### import libraries
from astropy.io import fits
import numpy as np
import os

import PyQt5
from PyQt5.QtWidgets import (QLabel, QPushButton, QComboBox, QRadioButton, QButtonGroup, QFrame, QGridLayout)
from PyQt5.QtGui import (QFont, QCursor)
from PyQt5.QtCore import Qt

import pyqtgraph as pg
from pyqtgraph.Qt import (QtCore, QtGui)
from pyqtgraph.Point import Point

# KOOLS reduction modules
from koolsred import (const_var, common_func, data_red)
from koolsred.gui import custom_window # for pyqtgraph

## define spectrum extraction flat box
class boxNoGrismImage(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        self.peak_file = 'lib/peak_pos_no_grism.dat'

        self.peak_xy_list = []
        self.peak_norm_list = []

# make buttons and input/output boxes
        miniLabelFrame = QLabel('Reference frame')
        miniLabelFrame.setFont(const_var.font)

        self.comboFrame = QComboBox()
        self.comboFrame.setFont(const_var.font)
#    for tmp_frame_id in const_var.frames['no_grism']:
#      self.comboGrism.addItem(tmp_frame_id)

        self.buttonOpenFrame = QPushButton('Open frame')
        self.buttonOpenFrame.clicked.connect(self.openFrame)
        self.buttonOpenFrame.setFont(const_var.font)
        self.buttonOpenFrame.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        miniLabelNormalize = QLabel('Normalize')
        miniLabelNormalize.setFont(const_var.font)
        radioNormalizeYes = QRadioButton('Yes')
        radioNormalizeYes.setFont(const_var.font)
        radioNormalizeYes.setChecked(True)
        radioNormalizeNo = QRadioButton('No')
        radioNormalizeNo.setFont(const_var.font)
        radioNormalizeNo.setChecked(False)
        self.groupNormalize = QButtonGroup()
        self.groupNormalize.addButton(radioNormalizeYes, 1)
        self.groupNormalize.addButton(radioNormalizeNo, 2)

        self.buttonExportPosCount = QPushButton('Export peak positions and count')
        self.buttonExportPosCount.clicked.connect(self.exportPeakPosCount)
        self.buttonExportPosCount.setFont(const_var.font)
        self.buttonExportPosCount.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.buttonReconstructImage = QPushButton('Reconstruct image')
        self.buttonReconstructImage.clicked.connect(self.reconstructImage)
        self.buttonReconstructImage.setFont(const_var.font)
        self.buttonReconstructImage.setStyleSheet('QWidget { background-color: #B0FFB0 }')

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(miniLabelFrame, 0, 0)
        layout.addWidget(self.comboFrame, 0, 1)
        layout.addWidget(self.buttonOpenFrame, 0, 2)

        layout.addWidget(miniLabelNormalize, 1, 0)
        layout.addWidget(radioNormalizeYes, 1, 1)
        layout.addWidget(radioNormalizeNo, 1, 2)

        layout.addWidget(self.buttonExportPosCount, 2, 0, 1, 2)
        layout.addWidget(self.buttonReconstructImage, 2, 2)

        self.setLayout(layout)

### functions
## PyQtGraph and its association
    def openFrame(self):
# set the input frame
        input_frame = common_func.add_suffix([self.comboFrame.currentText()], '-bias')[0]

        if not os.path.isfile(input_frame):
            print('Cannot open the input frame. {} does not exist.'.format(input_frame))
            return 1

# PyQtGraph
        pg.setConfigOptions(imageAxisOrder='row-major')

        pg.mkQApp()
#    self.win = pg.GraphicsLayoutWidget()
        self.win = custom_window.CustomGraphicsWindow()
        self.win.setWindowTitle('Reconstruct image (no grism): {}'.format(input_frame))

# A plot area (ViewBox + axes) for displaying the image
        self.p1 = self.win.addPlot()
        self.p1.setAspectLocked(True)

# Item for displaying image data
        self.img = pg.ImageItem()
        self.p1.addItem(self.img)

# Custom ROI for selecting an image region
        self.roi = pg.ROI([950, 300], [60, 1050])
        self.roi.addScaleHandle([0.5, 1], [0.5, 0.5])
        self.roi.addScaleHandle([0, 0.5], [0.5, 0.5])
        self.p1.addItem(self.roi)
        self.roi.setZValue(10)  # make sure ROI is drawn above image

# Contrast/color control
        hist = pg.HistogramLUTItem()
        hist.setImageItem(self.img)
        self.win.addItem(hist)

        self.win.resize(1200, 800)
        self.win.show()

# Generate image data
        self.data = fits.getdata(input_frame, view = np.ndarray)
        self.img.setImage(self.data)
        hist.setLevels(self.data.min(), self.data.max())

# Callbacks for handling user interaction
#    self.updatePlotExtract()
#    self.roi.sigRegionChanged.connect(self.updatePlotExtract)

        def keyPressed(event):
            if event.key() == Qt.Key_F:
                self.findPeaks()
            elif event.key() == Qt.Key_Question:
                self.showHelp()

        self.win.sigKeyPress.connect(keyPressed)

#  def updatePlotExtract(self):
#    selected = self.roi.getArrayRegion(self.data, self.img)

    def reload_p1(self):
        self.p1.clear()
        self.p1.addItem(self.img)
        self.p1.addItem(self.roi)

    def showHelp(self):
        print('''
      Help for making an image from a frame without grism.
        f -- find peak positions and make an image.
        ? -- show this help.
        ''')

    def plotPeakPoints(self):
# slice the peak position list
        peak_xy_array = np.array(self.peak_xy_list)
# plot peak points
        plot_x = (peak_xy_array[:, 0] + 0.5).tolist()
        plot_y = (peak_xy_array[:, 1] + 0.5).tolist()

        self.reload_p1()
        self.p1.addItem(pg.PlotDataItem(x=plot_x, y=plot_y, pen=None, symbol='x', symbolBrush=(0,255,0)))

## reduction functions
    def findPeaks(self):
# find the peak positions
        self.peak_xy_list = data_red.no_grism_image.findPeak_XY(self.data, self.roi.pos(), self.roi.size())
        self.plotPeakPoints()

# measure the normalized peak counts
        peak_count_list = data_red.no_grism_image.measureCount(self.data, self.peak_xy_list)
        mean = sum(peak_count_list) / len(peak_count_list)
        self.peak_norm_list = [i / mean for i in peak_count_list]

# export the peak positions and the normalized counts
        self.exportPeakPosCount()

    def reconstructImage(self):
        if not os.path.isfile(self.peak_file):
            print('The peak position file is not found.')
            return 1

        print('Image reconstruction starts.')

        input_list = common_func.add_suffix(const_var.frames['no_grism'], '-bias')
        output_list = common_func.add_suffix(const_var.frames['no_grism'], '-bias-image')

        if self.groupNormalize.checkedId() == 1:
            norm_flag = True
        else:
            norm_flag = False

        for input_fits, output_fits in zip(input_list, output_list):
            if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                data_red.no_grism_image.reconstruct_image(input_fits, output_fits, self.peak_file, norm_flag)
#        print('{} --> {}.'.format(input_fits, output_fits))

        print('Image reconstruction finished.')

## small functions
    def exportPeakPosCount(self):
        if not os.path.isdir('lib'):
            os.mkdir('lib')

        with open(self.peak_file, 'w') as fout:
            for line in zip(self.peak_xy_list, self.peak_norm_list):
                fout.write('{:.2f} {:.2f} {:.6f}\n'.format(line[0][0], line[0][1], line[1]))

        print('Export {}.'.format(self.peak_file))

    def updateGUI(self):
# update the frame list
        self.comboFrame.clear()
        for tmp_frame_id in const_var.frames['no_grism']:
            self.comboFrame.addItem(tmp_frame_id)
