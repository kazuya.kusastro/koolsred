#!/usr/bin/python3
# coding: utf-8

### gui/custom_window.py: define a custom window class which can emit key press events

# import libraries
import pyqtgraph as pg
from pyqtgraph.Qt import (QtCore, QtGui)

# define a custom window class
class CustomGraphicsWindow(pg.GraphicsWindow):
    sigKeyPress = QtCore.pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def keyPressEvent(self, event):
        self.scene().keyPressEvent(event)
        self.sigKeyPress.emit(event)
