#!/usr/bin/python3
# coding: utf-8

### gui/open_fits.py: GUI for opening a fits file

### import libraries
from astropy.io import fits
import numpy as np
import os

import PyQt5
from PyQt5.QtWidgets import (QPushButton, QFileDialog, QFrame, QGridLayout)
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

import pyqtgraph as pg
from pyqtgraph.Qt import (QtCore, QtGui)
from pyqtgraph.Point import Point

# KOOLS reduction modules
from koolsred import const_var
from koolsred.gui import custom_window # for pyqtgraph

## define spectrum extraction flat box
class boxOpenFits(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize

# make buttons and input/output boxes
        self.buttonOpenFrame = QPushButton('Open frame')
        self.buttonOpenFrame.clicked.connect(self.openFrame)
        self.buttonOpenFrame.setFont(const_var.font)
        self.buttonOpenFrame.setStyleSheet('QWidget { background-color: #B0FFB0 }')

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(self.buttonOpenFrame, 0, 0)

        self.setLayout(layout)

### define functions
    def openFrame(self):
# set the input frame
        file_filter = 'Fits files(*.fits);; All files(*)'
        self.file_name = QFileDialog.getOpenFileName(self, 'Open file', './', file_filter)[0]
        if not os.path.isfile(self.file_name):
            return 0

## PyQtGraph
        pg.setConfigOptions(imageAxisOrder='row-major')

        pg.mkQApp()
#    self.win = pg.GraphicsLayoutWidget()
        self.win = custom_window.CustomGraphicsWindow()
        self.win.setWindowTitle(self.file_name)

# A plot area (ViewBox + axes) for displaying the image
        self.p1 = self.win.addPlot()
        self.p1.setAspectLocked(True)

# Item for displaying image data
        self.img = pg.ImageItem()
        self.p1.addItem(self.img)

# Contrast/color control
        hist = pg.HistogramLUTItem()
        hist.setImageItem(self.img)
        self.win.addItem(hist)

        self.win.resize(800, 600)
        self.win.show()

# Generate image data
        self.data = fits.getdata(self.file_name, view = np.ndarray)
        self.img.setImage(self.data)
        hist.setLevels(self.data.min(), self.data.max())

# Callbacks for handling user interaction
        def keyPressed(event):
            if event.key() == Qt.Key_R:
                self.reloadFrame()
            elif event.key() == Qt.Key_Question:
                self.showHelp()

        self.win.sigKeyPress.connect(keyPressed)

    def showHelp(self):
        print('''
      Help for showing an image.
        r -- reload the frame.
        ? -- show this help.
        ''')

    def reloadFrame(self):
        self.p1.clear()
        self.data = fits.getdata(self.file_name, view = np.ndarray)
        self.p1.addItem(self.img)
