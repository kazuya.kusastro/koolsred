#!/usr/bin/python3
# coding: utf-8

### gui/sky_subtract.py: GUI for sky subtraction

### import libraries
import math
import multiprocessing
import operator
import os
import re
import sys
import time

from astropy.io import fits
import numpy as np
#from scipy import signal, interpolate, integrate

import PyQt5
from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow, QTabWidget, QTableWidget, QTableWidgetItem, QLabel, QLineEdit, QTextEdit, QPushButton, QRadioButton, QButtonGroup, QComboBox, QCheckBox, QMessageBox, QFileDialog, QFrame, QVBoxLayout, QGridLayout)
from PyQt5.QtGui import (QFont, QCursor)
from PyQt5.QtCore import (Qt, QTimer)

import pyqtgraph as pg
from pyqtgraph.Qt import (QtCore, QtGui)
from pyqtgraph.Point import Point

# KOOLS reduction modules
from koolsred import (const_var, common_func, data_red)
from koolsred.gui import custom_window # for pyqtgraph

### define constants


## define wavelength calibration box
class Frame(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        headerTable = ('frame ID', 'lambda_1 [pix]', 'lambda_2 [pix]', 'sky fiber')

#    self.grism_id = ''
        self.grism = ''
        self.frame_row_pg = 0
        self.input_fits = ''
        self.data_before = []
        self.data_after = []
        self.mouse_x = self.mouse_y = 0

# make buttons and input/output boxes
        mini_label_grism = QLabel('Grism')
        mini_label_grism.setFont(const_var.font)
        self.combo_grism = QComboBox()
        self.combo_grism.setFont(const_var.font)
        self.combo_grism.currentIndexChanged.connect(self.change_grism)

        self.button_import_file = QPushButton('Import file')
        self.button_import_file.clicked.connect(self.import_file)
        self.button_import_file.setFont(const_var.font)
        self.button_import_file.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        mini_label_sky_num = QLabel('Number of sky fibers')
        mini_label_sky_num.setFont(const_var.font)
        self.box_sky_num = QLineEdit()
        self.box_sky_num.setFont(const_var.font)
        self.box_sky_num.setAlignment(Qt.AlignRight)
        self.box_sky_num.setText('10')
        self.box_sky_num.setMaxLength(3)
        self.box_sky_num.editingFinished.connect(lambda: self.change_sky_num(0))

        self.button_sky_num_plus = QPushButton('+')
        self.button_sky_num_plus.clicked.connect(lambda: self.change_sky_num(1))
        self.button_sky_num_plus.setFont(const_var.font)
        self.button_sky_num_minus = QPushButton('-')
        self.button_sky_num_minus.clicked.connect(lambda: self.change_sky_num(-1))
        self.button_sky_num_minus.setFont(const_var.font)

        self.table_sky = QTableWidget(0, 4)
        self.table_sky.setHorizontalHeaderLabels(headerTable)
        self.table_sky.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}')
        self.table_sky.currentCellChanged.connect(self.read_table_value)

        self.button_open_pg = QPushButton('Open window')
        self.button_open_pg.clicked.connect(self.open_pg_window)
        self.button_open_pg.setFont(const_var.font)
        self.button_open_pg.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.button_select_sky = QPushButton('Select sky fibers')
        self.button_select_sky.clicked.connect(self.select_sky_gui)
        self.button_select_sky.setFont(const_var.font)
        self.button_select_sky.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.button_export_file = QPushButton('Export file')
        self.button_export_file.clicked.connect(self.export_file)
        self.button_export_file.setFont(const_var.font)
        self.button_export_file.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.buttonWaveCalib = QPushButton('Execute wavelength calibration')
        self.buttonWaveCalib.clicked.connect(self.waveCalib)
        self.buttonWaveCalib.setFont(const_var.font)
        self.buttonWaveCalib.setStyleSheet('QWidget { background-color: #B0FFB0 }')

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(mini_label_grism, 0, 0)
        layout.addWidget(self.combo_grism, 0, 1)
        layout.addWidget(self.button_import_file, 0, 2, 1, 2)

        layout.addWidget(mini_label_sky_num, 1, 0)
        layout.addWidget(self.box_sky_num, 1, 1)
        layout.addWidget(self.button_sky_num_plus, 1, 2)
        layout.addWidget(self.button_sky_num_minus, 1, 3)

        layout.addWidget(self.table_sky, 3, 0, 4, 4)

        layout.addWidget(self.button_open_pg, 7, 0)
        layout.addWidget(self.button_select_sky, 7, 1)
        layout.addWidget(self.button_export_file, 7, 2)
        layout.addWidget(self.buttonWaveCalib, 7, 3)

        self.setLayout(layout)

### function
## functions related to PyQtGraph
    def open_pg_window(self):
# record the table line ID
        if self.table_sky.currentItem() is None:
            return

        self.frame_row_pg = self.table_sky.currentRow()

# unfocus the table for writing the updated values in the table
        self.table_sky.clearFocus()

# read the fits file
        frame_id = self.frame_lam_sky[self.frame_row_pg][0]
        self.input_fits = '{}{:08d}-wc-norm.fits'.format(const_var.frameFormat.split('%08d')[0], frame_id)
        self.data_before = fits.getdata(self.input_fits, view=np.ndarray)

# define the main window
        pg.setConfigOptions(imageAxisOrder='row-major')
        pg.mkQApp()

        self.pg_win = custom_window.CustomGraphicsWindow()
        self.pg_win.resize(1200, 800)
        self.pg_win.setWindowTitle('Sky subtraction: {}'.format(self.input_fits))

## define the comparison image window
# before sky subtraction
        self.image1 = self.pg_win.addPlot(row=0, col=0)
        self.img = pg.ImageItem()
        self.image1.addItem(self.img)

        hist1 = pg.HistogramLUTItem()
        hist1.setImageItem(self.img)
        self.pg_win.addItem(hist1, row=0, col=1)

        self.img.setImage(self.data_before)
        hist1.setLevels(self.data_before.min(), self.data_before.max())

        self.line1 = pg.InfiniteLine(self.frame_lam_sky[self.frame_row_pg][1], movable=True)
        self.line1.sigPositionChangeFinished.connect(self.update_line_pos)
        self.image1.addItem(self.line1)

        self.line2 = pg.InfiniteLine(self.frame_lam_sky[self.frame_row_pg][2], movable=True)
        self.line2.sigPositionChangeFinished.connect(self.update_line_pos)
        self.image1.addItem(self.line2)

# after sky subtraction
        self.data_after = self.data_before

        self.image2 = self.pg_win.addPlot(row=1, col=0)
        self.img = pg.ImageItem()
        self.image2.addItem(self.img)

        hist2 = pg.HistogramLUTItem()
        hist2.setImageItem(self.img)
        self.pg_win.addItem(hist2, row=1, col=1)

        self.img.setImage(self.data_after)
        hist2.setLevels(self.data_after.min(), self.data_after.max())

        def keyPressed(event):
            if event.key() == Qt.Key_S:
                self.select_sky_pg()
            elif event.key() == Qt.Key_Plus:
                self.change_sky_num(1)
            elif event.key() == Qt.Key_Minus:
                self.change_sky_num(-1)
            elif event.key() == Qt.Key_Question:
                self.showHelp()

        self.pg_win.sigKeyPress.connect(keyPressed)

    def showHelp(self):
        print('''
      Help for the wavelength calibration window.
        d -- delete the sky fiber
        m -- mark the sky fiber
        s -- select sky fibers automatically
        + -- increase the number of sky fibers
        - -- decrease the number of sky fibers
        ? -- show this help
        ''')

    def open_fits(self):
        if self.table_sky.currentItem() is None:
            return

        frame_id = self.frame_lam_sky[self.table_sky.currentRow()][0]
        self.input_fits = '{}{:08d}-wc-norm.fits'.format(const_var.frameFormat.split('%08d')[0], frame_id)
#    print(input_fits)
        self.data_before = fits.getdata(self.input_fits, view=np.ndarray)

        return

    def update_line_pos(self):
        self.frame_lam_sky[self.frame_row_pg][1] = int(self.line1.value())
        self.frame_lam_sky[self.frame_row_pg][2] = int(self.line2.value())

        self.write_table_value()
        self.read_table_value()

        return

    def redraw_lines(self):
        self.line1.setValue(self.frame_lam_sky[self.frame_row_pg][1])
        self.line2.setValue(self.frame_lam_sky[self.frame_row_pg][2])

        return

    def select_sky_pg(self):
        self.write_sky_fiber(self.frame_row_pg)

        return

    def loadWindow(self):
        self.readTable()

        if hasattr(self, 'i1'):
            self.clearMarks()
            self.loadTitle()
            self.loadSpec()
            self.drawMarks()

            self.plotPosLam()

    def clearMarks(self):
        self.s2.clear()
        self.s2.addItem(self.region, ignoreBounds=True)
        self.s2.plot(self.data)

    def loadTitle(self):
        self.pg_win.setWindowTitle('Wavelength calibration: Grism = {} Fiber = {} Lamp = {}'.format(self.combo_grism.currentText(), self.fiberID, self.comboComp.currentText()))

    def loadSpec(self):
        self.fiberID = data_red.data_func.checkFiberID(int(self.box_sky_num.text()))
        self.data = self.data_whole[self.fiberID - 1, :].tolist()

        minX, maxX = self.region.getRegion()
        self.s1.setXRange(minX, maxX, padding=0)

    def drawMarks(self):
        lamp = self.comboComp.currentText()

# draw marks in image
        peak_x_list = []
        peak_y_list = []

        for fiber_count, tmp_list in enumerate(self.line_pos_lam):
            plot_list = [i[1] + const_var.plot_shift for i in tmp_list if i[0] == lamp]
            peak_x_list.extend(plot_list)
            peak_y_list.extend([fiber_count + const_var.plot_shift] * len(plot_list))

        self.i1.plot(x=peak_x_list, y=peak_y_list, pen=None, symbol='o', symbolSize=3, symbolBrush=(0, 255, 0))

# draw marks in spectrum
        for tmp_line in self.line_pos_lam[self.fiberID - 1]:
            if tmp_line[0] == lamp:
                x_int = int(float(tmp_line[1]))
                if 0 <= x_int < len(self.data) - 1:
                    peak_count = max(self.data[x_int], self.data[x_int + 1])
                    self.s2.plot(x=[tmp_line[1]], y=[peak_count], pen=None, symbol='t', symbolBrush=(0, 255, 0))

    def plotPosLam(self):
        self.g1.clear()
        self.g2.clear()

        tmp_line = self.line_pos_lam[self.fiberID - 1]
#    print('fiberID = {} line_pos_lms = {}'.format(self.fiberID, tmp_line))
        if len(tmp_line) > 0:
            peak_array = np.array([i[1:3] for i in tmp_line], dtype='float')
            self.g1.plot(peak_array, pen=None, symbol='+', symbolBrush=(255, 255, 255))

        if len(tmp_line) > 3:
            x_resi_array, self.f_cheby = data_red.wave_calib.fitResidual(peak_array)
            self.g2.plot(x_resi_array, pen=None, symbol='+', symbolBrush=(255, 255, 255))

## functions for data reduction
    def write_sky_fiber(self, frame_row):
        input_fits = '{}{:08d}-wc-norm.fits'.format(const_var.frameFormat.split('%08d')[0], self.frame_lam_sky[frame_row][0])
        lam1, lam2 = self.frame_lam_sky[frame_row][1:3]
        sky_num = int(self.box_sky_num.text())

        sky_fiber_list = sorted(data_red.sky_subtract.select_sky_fiber(input_fits, lam1, lam2)[:sky_num])
        self.frame_lam_sky[frame_row][3] = ','.join([str(i) for i in sky_fiber_list])

        self.write_table_value()

        return

    def markPeak(self):
        peak_x = data_red.wave_calib.measurePeak(self.data, int(self.mouse_x))

        lamp = self.comboComp.currentText()
        grism_id = const_var.grism_names.index(self.combo_grism.currentText())

        if hasattr(self, 'f_cheby'):
            near_id = 0
            for line_id, tmp_lam in enumerate(linelists[lamp]):
                if -2 < (self.f_cheby(peak_x) - tmp_lam) < 2:
                    near_id = line_id
            self.line_pos_lam[self.fiberID - 1].append([lamp, peak_x, linelists[lamp][near_id]])
        else:
            self.line_pos_lam[self.fiberID - 1].append([lamp, peak_x, linelists[lamp][0]])

        self.writeTable()

    def markPeakAll(self):
        self.readTable()
        grism = self.combo_grism.currentText().lower()

        for tmp_lamp in const_var.comp_names:
            if not os.path.isfile('{}-{}-extract.fits'.format(tmp_lamp, grism)):
                continue

            input_fits = '{}-{}-extract.fits'.format(tmp_lamp, grism)
            data_whole = fits.getdata(input_fits, view = np.ndarray)

            for tmp_line in self.line_pos_lam[self.fiberID - 1]:
                if tmp_line[0] != tmp_lamp:
                    continue

                peak_xy = data_red.wave_calib.markPeakAll(data_whole, round(tmp_line[1]), self.fiberID - 1)

                for tmp_peak_x, tmp_fiber in peak_xy:
                    for tmp2_line in self.line_pos_lam[tmp_fiber]:
                        if abs(tmp_peak_x - tmp2_line[1]) < min_peak_sep:
                            break
                    else:
                        self.line_pos_lam[tmp_fiber].append([tmp_line[0], tmp_peak_x, tmp_line[2]])

        self.writeTable()

## deta reduction functions
    def find_sky_fiber(self):
        pass

#    self.frame_lam_sky[

#    return

    def waveCalib(self):
# get grism ID
        grism = self.combo_grism.currentText().lower()
        grism_names_lower = [i.lower() for i in const_var.grism_names]

        if grism in grism_names_lower:
            grism_id = grism_names_lower.index(grism)
        else:
            print('Grism name does not match.')
            return

        input_list = common_func.add_suffix(const_var.frames['object'][grism_id], '-extract') + ['flat-{}-extract.fits'.format(grism)]
#    output_list = common_func.add_suffix(const_var.frames['object'][grism_id], '-wc')
        file_lib = 'lib/line-pos-lam-{}.dat'.format(grism)

        print('Wavelength calibration starts.')

# fork process
        num_cpu = int(const_var.cpuThreadLimit)
        input_list_div = common_func.divide_frame_list(input_list, num_cpu)

        def reduction(in_list):
            output_list = [i.replace('-extract.fits', '-wc.fits') for i in in_list]

            for input_fits, output_fits in zip(in_list, output_list):
                if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                    data_red.wave_calib.waveCalib(input_fits, output_fits, grism, file_lib)

        jobs = []
        for tmp_input in input_list_div:
            job = multiprocessing.Process(target=reduction, args=(tmp_input,))
            jobs.append(job)
            job.start()

        [job.join() for job in jobs]

# make flat frame
        data_red.wave_calib.makeFlat('flat-{}-wc.fits'.format(grism), 'lib/norm-flat-{}.fits'.format(grism))

        for tmp_input in input_list:
            input_fits = tmp_input.replace('-extract.fits', '-wc.fits')
            output_fits = tmp_input.replace('-extract.fits', '-wc-norm.fits')
            if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                data_red.data_func.arith_frame(input_fits, 'div', 'lib/norm-flat-{}.fits'.format(grism), output_fits)
                data_red.data_func.make3DImage(output_fits)

        print('Wavelength calibration finished.')

## small functions
    def update_grism_list(self):
        self.combo_grism.clear()

        for grism_id, grism_name in enumerate(const_var.grism_names):
            if len(const_var.frames['object'][grism_id]) > 0:
                self.combo_grism.addItem(grism_name)

        return

    def change_grism(self):
        grism = self.combo_grism.currentText()
        if grism == '':
            return

        grism_id = const_var.grism_names.index(grism)

        self.frame_lam_sky = []

        match = re.compile(r'\d+')
        for frame_name in const_var.frames['object'][grism_id]:
            frame_name_wc = frame_name.replace('.fits', '-wc-norm.fits')
            if os.path.isfile(frame_name_wc):
                frame_id = int(match.search(frame_name).group())
                self.frame_lam_sky.append([frame_id, 100, 200, ''])

        self.create_table()

        return

    def change_sky_num(self, diff):
        tmp_num = int(self.box_sky_num.text()) + diff

        if tmp_num < 1:
            tmp_num = 1
        elif tmp_num > const_var.n_fiber - 1:
            tmp_num = const_var.n_fiber - 1

        self.box_sky_num.setText(str(tmp_num))

        return

    def create_table(self):
        if self.table_sky.rowCount() > 0:
            self.table_sky.setRowCount(0)

        for count_row, tmp_list in enumerate(self.frame_lam_sky):
            self.table_sky.insertRow(count_row)

            for count_column in range(4):
                item = QTableWidgetItem(str(tmp_list[count_column]))
                if count_column == 0:
                    item.setFlags(Qt.ItemIsEnabled)
                self.table_sky.setItem(count_row, count_column, item)

        return

    def write_table_value(self):
        for count_row, tmp_list in enumerate(self.frame_lam_sky):
            for count_column in range(1, 4):
                self.table_sky.item(count_row, count_column).setText(str(tmp_list[count_column]))

        return

    def read_table_value(self):
# initialize the list and the flag
        self.frame_lam_sky = []
        swap_flag = False

        for count in range(self.table_sky.rowCount()):
# read values in the table
            frame_id = int(self.table_sky.item(count, 0).text())
            lam1 = int(self.table_sky.item(count, 1).text())
            lam2 = int(self.table_sky.item(count, 2).text())
            sky_fiber_str = self.table_sky.item(count, 3).text()

# swap lam1 and lam2 if lam1 > lam2
            if lam1 > lam2:
                lam1, lam2 = lam2, lam1
                swap_flag = True

# append the list
            self.frame_lam_sky.append([frame_id, lam1, lam2, sky_fiber_str])

# write the table if swapped
        if swap_flag:
            self.write_table_value()

# redraw the lines if the pyqtgraph window exists
        if hasattr(self, 'pg_win'):
            self.redraw_lines()

        return

    def select_sky_gui(self):
        if self.table_sky.currentItem() is not None:
            self.write_sky_fiber(self.table_sky.currentRow())

        return

    def import_file(self):
        file_in = 'lib/sky-subtract-{}.dat'.format(self.combo_grism.currentText().lower())
        if not os.path.isfile(file_in):
            print('Sky data file is not found.')
            return

        self.frame_lam_sky = []

        with open(file_in, 'r') as fin:
            input_lines = fin.readlines()

        print('Import {}'.format(file_in))

        match = re.compile(r'\d+')

        for tmp_line in input_lines:
            line_split = tmp_line.rstrip('\n').split(' ')

            if len(line_split) >= 3:
                frame_id = int(match.search(line_split[0]).group())
                lam_1 = int(match.search(line_split[1]).group())
                lam_2 = int(match.search(line_split[2]).group())
                if len(line_split) >= 4:
                    sky_fiber = ' '.join(line_split[3:])
                else:
                    sky_fiber = ''

                self.frame_lam_sky.append([frame_id, lam_1, lam_2, sky_fiber])

        self.create_table()

        return

    def export_file(self):
        file_out = 'lib/sky-subtract-{}.dat'.format(self.combo_grism.currentText().lower())
        with open(file_out, 'w') as fout:
            for tmp_line in self.frame_lam_sky:
                fout.write('{0[0]} {0[1]} {0[2]} {0[3]}\n'.format(tmp_line))

        print('Export {}'.format(file_out))

        return

    def update_gui(self):
        self.update_grism_list()
