#!/usr/bin/python3
# coding: utf-8

### gui/extract_spec.py: GUI for extracting spectra using flat frames

### import libraries
from astropy.io import fits
import math
import multiprocessing
import numpy as np
import os
import sys

import PyQt5
from PyQt5.QtWidgets import (QTableWidget, QTableWidgetItem, QLabel, QLineEdit, QTextEdit, QPushButton, QComboBox, QFrame, QGridLayout)
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt

import pyqtgraph as pg
from pyqtgraph.Qt import (QtCore, QtGui)
from pyqtgraph.Point import Point

# KOOLS reduction modules
from koolsred import const_var, common_func, data_red
from koolsred.gui import custom_window # for pyqtgraph

## define spectrum extraction flat box
class boxExtractSpec(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        headerTable = ('x [pixel]', 'y [pixel]')

        self.fiberbundle_id = '2014'
        self.n_fiber = const_var.n_fiber_dict[self.fiberbundle_id]

        self.init_x = 0
        self.init_y_list = []
        self.combine_y_pix = 5
        self.peak_xy_list = [[] for i in range(self.n_fiber)]

        self.poly_coeff = []

# make buttons and input/output boxes
        miniLabelGrism = QLabel('Grism')
        miniLabelGrism.setFont(const_var.font)
        self.comboGrism = QComboBox()
        self.comboGrism.setFont(const_var.font)

        self.buttonImportPeakFile = QPushButton('Import peak positions')
        self.buttonImportPeakFile.clicked.connect(self.importPeakFile)
        self.buttonImportPeakFile.setFont(const_var.font)
        self.buttonImportPeakFile.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.buttonOpenFlat = QPushButton('Open flat frame')
        self.buttonOpenFlat.clicked.connect(self.openFlatWindow)
        self.buttonOpenFlat.setFont(const_var.font)
        self.buttonOpenFlat.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        miniLabelFiberID = QLabel('Fiber ID')
        miniLabelFiberID.setFont(const_var.font)
        self.boxFiberID = QLineEdit()
        self.boxFiberID.setFont(const_var.font)
        self.boxFiberID.setAlignment(Qt.AlignRight)
        self.boxFiberID.setText('1')
        self.boxFiberID.setMaxLength(3)
        self.boxFiberID.editingFinished.connect(lambda: self.changeFiberID(0))

        self.buttonFiberIDPlus = QPushButton('+')
        self.buttonFiberIDPlus.clicked.connect(lambda: self.changeFiberID(1))
        self.buttonFiberIDPlus.setFont(const_var.font)
#    self.buttonPlotPreviousPoints.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.buttonFiberIDMinus = QPushButton('-')
        self.buttonFiberIDMinus.clicked.connect(lambda: self.changeFiberID(-1))
        self.buttonFiberIDMinus.setFont(const_var.font)
#    self.buttonPlotNextPoints.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.buttonExportPeakFile = QPushButton('Export peak positions')
        self.buttonExportPeakFile.clicked.connect(self.exportPeakFile)
        self.buttonExportPeakFile.setFont(const_var.font)
        self.buttonExportPeakFile.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.buttonExtractSpectra = QPushButton('Extract spectra')
        self.buttonExtractSpectra.clicked.connect(self.extractSpec)
        self.buttonExtractSpectra.setFont(const_var.font)
        self.buttonExtractSpectra.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        miniLabelDeletePos = QLabel('Delete peak positions')
        miniLabelDeletePos.setFont(const_var.font)

        self.buttonDeletePeakPos = QPushButton('1 position')
        self.buttonDeletePeakPos.clicked.connect(self.deletePeakPos)
        self.buttonDeletePeakPos.setFont(const_var.font)
        self.buttonDeletePeakPos.setStyleSheet('QWidget { background-color: #FFB0B0 }')

        self.buttonDeletePeakAllPos = QPushButton('All positions')
        self.buttonDeletePeakAllPos.clicked.connect(self.deletePeakAllPos)
        self.buttonDeletePeakAllPos.setFont(const_var.font)
        self.buttonDeletePeakAllPos.setStyleSheet('QWidget { background-color: #FFB0B0 }')

        self.buttonDeletePeakAllFibers = QPushButton('All fibers')
        self.buttonDeletePeakAllFibers.clicked.connect(self.deletePeakAllFibers)
        self.buttonDeletePeakAllFibers.setFont(const_var.font)
        self.buttonDeletePeakAllFibers.setStyleSheet('QWidget { background-color: #FFB0B0 }')

        messageInstruction = QLabel('Hit "?" key on the graph window for help.')
        messageInstruction.setFont(const_var.font)

        self.tablePeaks = QTableWidget(0, 2)
        self.tablePeaks.setHorizontalHeaderLabels(headerTable)
        self.tablePeaks.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}')
#    self.tablePeaks.setColumnWidth(0, 120)
#    self.tablePeaks.setColumnWidth(1, 120)

# layout
        layout = QGridLayout()
        layout.setSpacing(10)

        layout.addWidget(miniLabelGrism, 0, 0, 1, 2)
        layout.addWidget(self.comboGrism, 0, 2, 1, 2)

        layout.addWidget(self.buttonImportPeakFile, 1, 0, 1, 2)
        layout.addWidget(self.buttonOpenFlat, 1, 2, 1, 2)

        layout.addWidget(miniLabelFiberID, 2, 0)
        layout.addWidget(self.boxFiberID, 2, 1)
        layout.addWidget(self.buttonFiberIDPlus, 2, 2)
        layout.addWidget(self.buttonFiberIDMinus, 2, 3)

        layout.addWidget(self.buttonExportPeakFile, 3, 0, 1, 2)
        layout.addWidget(self.buttonExtractSpectra, 3, 2, 1, 2)

        layout.addWidget(miniLabelDeletePos, 4, 0, 1, 4)
        layout.addWidget(self.buttonDeletePeakPos, 5, 0)
        layout.addWidget(self.buttonDeletePeakAllPos, 5, 1)
        layout.addWidget(self.buttonDeletePeakAllFibers, 5, 2, 1, 2)

        layout.addWidget(messageInstruction, 6, 0, 1, 4)

        layout.addWidget(self.tablePeaks, 0, 4, 7, 1)

        self.setLayout(layout)

### functions
## PyQtGraph and its association
    def openFlatWindow(self):
# input grism name
        grism = self.comboGrism.currentText()
        flat_frame = 'flat-{}-gain.fits'.format(grism).lower()

        if not os.path.isfile(flat_frame):
            print('Cannot open the flat frame. {} does not exist.'.format(flat_frame))
            return 1

# PyQtGraph
        pg.setConfigOptions(imageAxisOrder='row-major')

        pg.mkQApp()
#    self.win = pg.GraphicsLayoutWidget()
        self.win = custom_window.CustomGraphicsWindow()
        self.win.setWindowTitle('Extract spectra: {}'.format(flat_frame))

# A plot area (ViewBox + axes) for displaying the image
        self.p1 = self.win.addPlot()
        self.p1.setAspectLocked(True)

# Item for displaying image data
        self.img = pg.ImageItem()
        self.p1.addItem(self.img)

# Custom ROI for selecting an image region
        self.roi = pg.ROI([1001, 200], [10, 1300])
        self.roi.addScaleHandle([0.5, 1], [0.5, 0.5])
        self.roi.addScaleHandle([0, 0.5], [0.5, 0.5])
        self.p1.addItem(self.roi)
        self.roi.setZValue(10)  # make sure ROI is drawn above image

# Contrast/color control
        hist = pg.HistogramLUTItem()
        hist.setImageItem(self.img)
        self.win.addItem(hist)

        self.win.resize(1200, 800)
        self.win.show()

# Another plot area for displaying ROI data
        self.win.nextRow()
        self.p2 = self.win.addPlot(colspan=2)
#    p2.setMaximumWidth(250)
        self.p2.setMaximumHeight(300)
#    p2.rotate(330)

# input the fits file
        self.data, header = fits.getdata(flat_frame, view=np.ndarray, header=True)

# check the fiber version
        fiberbundle_id = header['FIBER-ID']
        if fiberbundle_id in const_var.n_fiber_dict:
            self.fiberbundle_id = fiberbundle_id

# Generate image data
        self.img.setImage(self.data)
        hist.setLevels(self.data.min(), self.data.max())

# Callbacks for handling user interaction
        self.updatePlotExtract()
        self.roi.sigRegionChanged.connect(self.updatePlotExtract)

        def keyPressed(event):
            if event.key() == Qt.Key_A:
                self.plotPeakPointsAll()
            elif event.key() == Qt.Key_E:
                self.exportPeakFile()
            elif event.key() == Qt.Key_F:
                self.findPeaks()
            elif event.key() == Qt.Key_I:
                self.importPeakFile()
            elif event.key() == Qt.Key_L:
                self.changeFiberID(0)
            elif event.key() == Qt.Key_N:
                self.changeFiberID(1)
            elif event.key() == Qt.Key_P:
                self.changeFiberID(-1)
            elif event.key() == Qt.Key_Question:
                self.showHelp()

        self.win.sigKeyPress.connect(keyPressed)

    def updatePlotExtract(self):
        selected = self.roi.getArrayRegion(self.data, self.img)
        self.p2.plot(selected.mean(axis=1), clear=True)

    def reload_p1(self):
        self.p1.clear()
        self.p1.addItem(self.img)
        self.p1.addItem(self.roi)

    def showHelp(self):
        print('''
      Help for spectrum extraction window.
        a -- plot peak positions of all the fibers
        e -- export the peak position file
        f -- find peak positions
        i -- import the peak position file
        l -- plot peak positions of the fiber ID
        n -- plot peak positions of the next fiber
        p -- plot peak positions of the previous fiber
        ? -- show this help
        ''')

    def plotPeakPoints(self):
# check weather the graph window appears
        if not hasattr(self, 'p1'):
            return 0

# check the size of self.peak_xy_list
        if len(self.peak_xy_list) == 0:
            return 0

# slice the peak position list
        fiberID = int(self.boxFiberID.text())
        if len(self.peak_xy_list[fiberID - 1]) == 0:
            return 0

# plot peak points
        plot_x = [i[0] for i in self.peak_xy_list[fiberID - 1]]
        plot_y = [i[1] for i in self.peak_xy_list[fiberID - 1]]

        self.reload_p1()
        self.p1.addItem(pg.PlotDataItem(x=plot_x, y=plot_y, pen=None, symbol='x', symbolBrush=(0,255,0)))

    def plotPeakPointsAll(self):
# check weather the graph window appears
        if not hasattr(self, 'p1'):
            return 0

# initialize
        self.reload_p1()
        brush_color = [(0,255,0), (255,0,0), (0,0,255), (255,255,255)]

# plot peak points
        for tmp_count, tmp_list in enumerate(self.peak_xy_list):
            plot_x = [i[0] + 0.5 for i in tmp_list]
            plot_y = [i[1] + 0.5 for i in tmp_list]

            self.p1.addItem(pg.PlotDataItem(x=plot_x, y=plot_y, pen=None, symbol='x', symbolBrush=brush_color[tmp_count % len(brush_color)]))

## reduction functions
    def findPeaks(self):
#    self.peak_xy_list = data_red.ExtractSpec.findPeak_XY(self.data, self.roi.pos(), self.roi.size())
        self.peak_xy_list = data_red.extract_spec.find_peak_xy2(self.data, self.fiberbundle_id)

        self.plotPeakPoints()
        self.changeFiberID(0)
        self.updateTable()

    def extractSpec(self):
        grism = self.comboGrism.currentText().lower()
        grism_names_lower = [i.lower() for i in const_var.grism_names]

        if grism in grism_names_lower:
            grism_id = grism_names_lower.index(grism)
        else:
            print('Grism name does not match.')
            return

#    grism_id = self.comboGrism.currentIndex()
#    grism = (const_var.grism_names[grism_id]).lower()

#    print(grism_id, grism)
        peak_file = 'lib/peak_pos_{}.dat'.format(grism)

        if not os.path.isfile(peak_file):
            self.exportPeakFile()

        print('Spectrum extraction starts.')

        input_list = common_func.add_suffix(const_var.frames['object'][grism_id], '-gain') + ['flat-{}-gain.fits'.format(grism)]
        for tmp_lamp in const_var.comp_names:
            comp_frame = '{}-{}-gain.fits'.format(tmp_lamp, grism)
            if os.path.isfile(comp_frame):
                input_list += [comp_frame]

#    output_list = common_func.add_suffix(const_var.frames['object'][grism_id], '-extract') + ['flat-{}-extract.fits'.format(grism).lower()]

# fork process
        num_cpu = int(const_var.cpuThreadLimit)
        input_list_div = common_func.divide_frame_list(input_list, num_cpu)

        def reduction(in_list):
            output_list = [i.replace('-gain.fits', '-extract.fits') for i in in_list]
#        output_list.append('{}'.format(tmp_input.replace('-gain.fits', '-extract.fits')))
            for input_fits, output_fits in zip(in_list, output_list):
                if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                    data_red.extract_spec.extract_fits(input_fits, output_fits, peak_file, num_cpu=1)
#          print('{} --> {}.'.format(input_fits, output_fits))
#        data_red.DataFunc.make3DImage(output_fits)

        jobs = []
        for tmp_input in input_list_div:
            job = multiprocessing.Process(target=reduction, args=(tmp_input,))
            jobs.append(job)
            job.start()

        [job.join() for job in jobs]

        print('Process of spectrum extraction finished.')

    def changeFiberID(self, diff):
        self.boxFiberID.setText(str(data_red.data_func.checkFiberID(int(self.boxFiberID.text()) + int(diff), self.fiberbundle_id)))
        self.updateTable()
        self.plotPeakPoints()

    def deletePeakPos(self):
        if self.tablePeaks.currentItem() is not None:
            row = self.tablePeaks.currentRow()
            del self.peak_xy_list[int(self.boxFiberID.text()) - 1][row]
            self.updateTable()

    def deletePeakAllPos(self):
        fiberID = int(self.boxFiberID.text())
        self.peak_xy_list[fiberID - 1] = []
        self.changeFiberID(0)

    def deletePeakAllFibers(self):
        self.peak_xy_list = [[] for i in range(self.n_fiber)]
        self.changeFiberID(0)

    def updateTable(self):
# initialize the table
        if len(self.peak_xy_list) < self.n_fiber:
            return

#    self.tablePeaks.clearContents()
        fiberID = int(self.boxFiberID.text())
        peak_xy_cut = self.peak_xy_list[fiberID - 1]
        self.tablePeaks.setRowCount(len(peak_xy_cut))

# input data on the table
        for count_row, tmp_xy in enumerate(peak_xy_cut):
            for count_column, tmp_value in enumerate(tmp_xy):
                item = QTableWidgetItem('{:.2f}'.format(tmp_value))
                self.tablePeaks.setItem(count_row, count_column, item)

    def importPeakFile(self):
        grism = self.comboGrism.currentText().lower()
        peak_file = 'lib/peak_pos_{}.dat'.format(grism)

        self.peak_xy_list = data_red.extract_spec.importPeakFile(peak_file)
        print('Import {}.'.format(peak_file))

        self.changeFiberID(0)
#    self.updateTable()

    def exportPeakFile(self):
        grism = self.comboGrism.currentText().lower()
        peak_file = 'lib/peak_pos_{}.dat'.format(grism)

        if not os.path.isdir('lib'):
            os.mkdir('lib')

        with open(peak_file, 'w') as fout:
#      for count_fiber, pos_list in enumerate(self.peak_xy_list):
#        fout.write('{} '.format(count_fiber + 1))
            for pos_list in self.peak_xy_list:
                for line in pos_list:
                    fout.write('{:.2f} {:.2f} '.format(line[0], line[1]))
                fout.write('\n')

        print('Export {}.'.format(peak_file))

    def updateGUI(self):
        self.comboGrism.clear()
        for tmp_grism in const_var.grism_names:
            if os.path.isfile('flat-{}-gain.fits'.format(tmp_grism.lower())) or os.path.isfile('lib/peak_pos_{}.dat'.format(tmp_grism.lower())):
                self.comboGrism.addItem(tmp_grism)
