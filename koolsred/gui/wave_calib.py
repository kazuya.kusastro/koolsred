#!/usr/bin/python3
# coding: utf-8

### gui/wave_calib.py: GUI for wavelength calibration

### import libraries
import math
import multiprocessing
import operator
import os
import sys
import time

from astropy.io import fits
import numpy as np
#from scipy import signal, interpolate, integrate

import PyQt5
from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow, QTabWidget, QTableWidget, QTableWidgetItem, QLabel, QLineEdit, QTextEdit, QPushButton, QRadioButton, QButtonGroup, QComboBox, QCheckBox, QMessageBox, QFileDialog, QFrame, QVBoxLayout, QGridLayout)
from PyQt5.QtGui import (QFont, QCursor)
from PyQt5.QtCore import (Qt, QTimer)

import pyqtgraph as pg
from pyqtgraph.Qt import (QtCore, QtGui)
from pyqtgraph.Point import Point

# KOOLS reduction modules
from koolsred import (const_var, common_func, data_red)
from koolsred.gui import custom_window # for pyqtgraph

### define constants
# line lists
linelists = {}
linelists['Hg'] = [4046.565, 4358.335, 4916.068, 5460.75, 5769.61, 5790.67, 6907.46, 7728.825, 10139.75]
#linelists['Ne'] = [5037.7512, 5116.5032, 5330.7775, 5341.0938, 5400.5616, 5852.4878, 5881.895, 5944.834, 5975.5343, 6029.9968, 6074.3376, 6096.1630, 6143.0627, 6163.5937, 6217.2812, 6266.4952, 6304.7893, 6334.4276, 6382.9914, 6402.2480, 6506.5277, 6532.8824, 6598.9528, 6678.2766, 6717.0430, 6929.4672, 7032.4128, 7173.9380, 7245.1665, 7438.8981, 7488.8712, 7943.1805, 8082.4576, 8136.4061, 8300.3248, 8377.6070, 8495.3591, 8634.6472, 8654.3828]
linelists['Ne'] = [5037.7512, 5341.0938, 5400.5616, 5852.4878, 5881.895, 5944.834, 5975.5343, 6029.9968, 6074.3376, 6096.1630, 6143.0627, 6163.5937, 6217.2812, 6266.4952, 6304.7893, 6334.4276, 6382.9914, 6402.2480, 6506.5277, 6532.8824, 6598.9528, 6678.2766, 6717.0430, 6929.4672, 7032.4128, 7173.9380, 7245.1665, 7438.8981, 7488.8712, 7943.1805, 8082.4576, 8136.4061, 8300.3248, 8377.6070, 8495.3591, 8634.6472, 8654.3828]
linelists['Xe'] = [7119.598, 7584.680, 7642.024, 7967.342, 8231.6336, 8280.1162, 8346.8217, 8409.1894, 8819.4106, 8952.2509, 9045.4466, 9162.6520, 9799.697, 9923.198]

min_peak_sep = 2

# for sorting the position list
sort_column = operator.itemgetter(1)

## define wavelength calibration box
class boxWaveCalib(QFrame):
    def __init__(self, parent=None):
        super(QFrame, self).__init__(parent)

# initialize
        headerTable = ('lamp', 'x [pixel]', 'lambda [A]')

        self.fiberbundle_id = '2014'
        self.n_fiber = const_var.n_fiber_dict['2014']

#    self.grism_id = ''
        self.grism = ''
        self.fiberID = int(self.n_fiber / 2 + 1)
        self.data_whole = []
        self.mouse_x = self.mouse_y = 0

        self.line_pos_lam = [[] for i in range(self.n_fiber)]
#    self.f_cheby = [0] * const_var.n_fiber

# make buttons and input/output boxes
        self.button = {}

        miniLabelGrism = QLabel('Grism')
        miniLabelGrism.setFont(const_var.font)
        self.comboGrism = QComboBox()
        self.comboGrism.setFont(const_var.font)
        self.comboGrism.currentIndexChanged.connect(self.changeGrism)

        self.buttonImportLines = QPushButton('Import line file')
        self.buttonImportLines.clicked.connect(self.importLines)
        self.buttonImportLines.setFont(const_var.font)
        self.buttonImportLines.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        miniLabelComp = QLabel('Comparison lamp')
        miniLabelComp.setFont(const_var.font)
        self.comboComp = QComboBox()
        self.comboComp.setFont(const_var.font)
#    self.comboComp.currentIndexChanged.connect(self.openCompFits)

        miniLabelFiberID = QLabel('Fiber ID')
        miniLabelFiberID.setFont(const_var.font)
        self.boxFiberID = QLineEdit()
        self.boxFiberID.setFont(const_var.font)
        self.boxFiberID.setAlignment(Qt.AlignRight)
        self.boxFiberID.setText('{}'.format(int(self.n_fiber / 2 + 1)))
        self.boxFiberID.setMaxLength(3)
        self.boxFiberID.editingFinished.connect(lambda: self.changeFiberID(0))
        self.buttonFiberIDPlus = QPushButton('+')
        self.buttonFiberIDPlus.clicked.connect(lambda: self.changeFiberID(1))
        self.buttonFiberIDPlus.setFont(const_var.font)
        self.buttonFiberIDMinus = QPushButton('-')
        self.buttonFiberIDMinus.clicked.connect(lambda: self.changeFiberID(-1))
        self.buttonFiberIDMinus.setFont(const_var.font)

        self.buttonOpenSpec = QPushButton('Open spectrum')
        self.buttonOpenSpec.clicked.connect(self.openGraphWindow)
        self.buttonOpenSpec.setFont(const_var.font)
        self.buttonOpenSpec.setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.tableLines = QTableWidget(0, 3)
        self.tableLines.setHorizontalHeaderLabels(headerTable)
        self.tableLines.setStyleSheet('QAbstractScrollArea {font-size: 12pt;}')

        self.button['refit_lines'] = QPushButton('Refit lines')
        self.button['refit_lines'].clicked.connect(self.refit_lines)
        self.button['refit_lines'].setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.button['delete_line'] = QPushButton('Delete line')
        self.button['delete_line'].clicked.connect(self.delete_line)
        self.button['delete_line'].setStyleSheet('QWidget { background-color: #FFB0B0 }')

        self.button['delete_lines'] = QPushButton('Delete lines in all fibers')
        self.button['delete_lines'].clicked.connect(self.delete_lines)
        self.button['delete_lines'].setStyleSheet('QWidget { background-color: #FFB0B0 }')

        self.button['delete_all_lines'] = QPushButton('Delete all lines')
        self.button['delete_all_lines'].clicked.connect(self.delete_all_lines)
        self.button['delete_all_lines'].setStyleSheet('QWidget { background-color: #FF0000 }')

        self.button['export'] = QPushButton('Export line file')
        self.button['export'].clicked.connect(self.exportLines)
        self.button['export'].setStyleSheet('QWidget { background-color: #B0FFB0 }')

        self.button['wave_calib'] = QPushButton('Execute wavelength calibration')
        self.button['wave_calib'].clicked.connect(self.waveCalib)
        self.button['wave_calib'].setStyleSheet('QWidget { background-color: #B0FFB0 }')

# set the common settings
        [self.button[i].setFont(const_var.font) for i in self.button]

# layout
        layout = QGridLayout()
        layout.setSpacing(10)
        count_row = 0

        layout.addWidget(miniLabelGrism, count_row, 0)
        layout.addWidget(self.comboGrism, count_row, 1)
        layout.addWidget(self.buttonImportLines,count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(miniLabelComp, count_row, 0)
        layout.addWidget(self.comboComp, count_row, 1)
        layout.addWidget(self.buttonOpenSpec, count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(miniLabelFiberID, count_row, 0)
        layout.addWidget(self.boxFiberID, count_row, 1)
        layout.addWidget(self.buttonFiberIDPlus, count_row, 2)
        layout.addWidget(self.buttonFiberIDMinus, count_row, 3)
        count_row += 1

        layout.addWidget(self.tableLines, count_row, 0, 6, 2)
        layout.addWidget(self.button['refit_lines'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.button['delete_line'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.button['delete_lines'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.button['delete_all_lines'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.button['export'], count_row, 2, 1, 2)
        count_row += 1

        layout.addWidget(self.button['wave_calib'], count_row, 2, 1, 2)

        self.setLayout(layout)

### function
## functions related to PyQtGraph
    def openGraphWindow(self):
# read the fits file
        self.readFits()

# define the main window
        pg.setConfigOptions(imageAxisOrder='row-major')
        pg.mkQApp()

        self.win = custom_window.CustomGraphicsWindow()
        self.win.resize(1200, 800)
        self.win.setWindowTitle('Wavelength calibration: Grism = {} Fiber = {} Lamp = {}'.format(self.comboGrism.currentText(), self.fiberID, self.comboComp.currentText()))

# define the comparison image window
        self.i1 = self.win.addPlot(row=0, col=0, colspan=2)
        self.i1.setMaximumHeight(200)
        self.i1.setAspectLocked(True)
        self.img = pg.ImageItem()
        self.i1.addItem(self.img)

        hist = pg.HistogramLUTItem()
        hist.setImageItem(self.img)
        hist.setMaximumHeight(200)
        self.win.addItem(hist, row=0, col=2)

        self.img.setImage(self.data_whole)
        hist.setLevels(self.data_whole.min(), self.data_whole.max())

# set the label
        label = pg.LabelItem(justify='right')
        self.win.addItem(label, row=1, col=0)

# define the comparison spectrum window
        self.s1 = self.win.addPlot(row=2, col=0)
        self.s2 = self.win.addPlot(row=3, col=0)

        self.region = pg.LinearRegionItem()
        self.region.setZValue(10)
# Add the LinearRegionItem to the ViewBox, but tell the ViewBox to exclude this
# item when doing auto-range calculations.
        self.s2.addItem(self.region, ignoreBounds=True)

        self.s1.setAutoVisible(y=True)

        self.fiberID = int(self.boxFiberID.text())
        self.data = self.data_whole[self.fiberID - 1, :]
        self.s1.plot(self.data)
        self.s2.plot(self.data)

        def update():
            self.region.setZValue(10)
            minX, maxX = self.region.getRegion()
            self.s1.setXRange(minX, maxX, padding=0)

        self.region.sigRegionChanged.connect(update)

        def updateRegion(window, viewRange):
            rgn = viewRange[0]
            self.region.setRegion(rgn)

        self.s1.sigRangeChanged.connect(updateRegion)
        self.region.setRegion([100, 200])

## cross hair
        vLine = pg.InfiniteLine(angle=90, movable=False)
        hLine = pg.InfiniteLine(angle=0, movable=False)
        self.s1.addItem(vLine, ignoreBounds=True)
        self.s1.addItem(hLine, ignoreBounds=True)

        vb = self.s1.vb

        def mouseMoved(event):
            pos = event
            if self.s1.sceneBoundingRect().contains(pos):
                mousePoint = vb.mapSceneToView(pos)
                if 0 <= mousePoint.x() < len(self.data):
                    if hasattr(self, 'f_cheby'):
                        label.setText('<span style="font-size: 12pt">x={:.1f}, lambda={:.1f}, y={:.1f}</span>'.format(mousePoint.x(), self.f_cheby(mousePoint.x()), self.data[int(mousePoint.x())]))
                    else:
                        label.setText('<span style="font-size: 12pt">x={:.1f}, y={:.1f}</span>'.format(mousePoint.x(), self.data[int(mousePoint.x())]))

                    vLine.setPos(mousePoint.x())
                    hLine.setPos(mousePoint.y())

                    self.mouse_x = mousePoint.x()

        self.s1.scene().sigMouseMoved.connect(mouseMoved)

# define the pixel-wavelength relation and the residual
        self.g1 = self.win.addPlot(row=2, col=1, colspan=2)
        self.g2 = self.win.addPlot(row=3, col=1, colspan=2)

# load graphs
        self.loadWindow()

        def keyPressed(event):
            if event.key() == Qt.Key_A:
                self.markPeakAll()
                self.loadWindow()
            elif event.key() == Qt.Key_E:
                self.exportLines()
            elif event.key() == Qt.Key_L:
                self.changeFiberID(0)
                self.loadWindow()
            elif event.key() == Qt.Key_M:
                self.markPeak()
                self.loadWindow()
            elif event.key() == Qt.Key_N:
                self.changeFiberID(1)
                self.loadWindow()
            elif event.key() == Qt.Key_P:
                self.changeFiberID(-1)
                self.loadWindow()
            elif event.key() == Qt.Key_R:
                self.loadWindow()
            elif event.key() == Qt.Key_Question:
                self.showHelp()

        self.win.sigKeyPress.connect(keyPressed)

    def showHelp(self):
        print('''
      Help for the wavelength calibration window.
        a -- mark the peaks in all the fibers
        l -- plot the spectrum of the fiber ID
        m -- mark the peaks
        n -- plot the spectrum of the next fiber
        p -- plot the spectrum of the previous fiber
        r -- redraw the graphs
        ? -- show this help
        ''')

## open or reload the spectrum window
    def readFits(self):
        input_fits = '{}-{}-extract.fits'.format(self.comboComp.currentText(), self.comboGrism.currentText().lower())
        self.data_whole, header = fits.getdata(input_fits, view=np.ndarray, header=True)

# check the fiber version
        fiberbundle_id = header.get('FIBER-ID', default='2014')
        if fiberbundle_id in const_var.n_fiber_dict:
            self.fiberbundle_id = fiberbundle_id
            self.n_fiber = const_var.n_fiber_dict[self.fiberbundle_id]
            self.line_pos_lam = self.line_pos_lam[:self.n_fiber]

        return

    def loadWindow(self):
        self.readTable()

        if hasattr(self, 'i1'):
            self.clearMarks()
            self.loadTitle()
            self.loadSpec()
            self.drawMarks()

            self.plotPosLam()

    def clearMarks(self):
        self.s2.clear()
        self.s2.addItem(self.region, ignoreBounds=True)
        self.s2.plot(self.data)

    def loadTitle(self):
        self.win.setWindowTitle('Wavelength calibration: Grism = {} Fiber = {} Lamp = {}'.format(self.comboGrism.currentText(), self.fiberID, self.comboComp.currentText()))

    def loadSpec(self):
        self.fiberID = data_red.data_func.checkFiberID(int(self.boxFiberID.text()))
        self.data = self.data_whole[self.fiberID - 1, :].tolist()

        minX, maxX = self.region.getRegion()
        self.s1.setXRange(minX, maxX, padding=0)

    def drawMarks(self):
        lamp = self.comboComp.currentText()

# draw marks in image
        peak_x_list = []
        peak_y_list = []

        for fiber_count, tmp_list in enumerate(self.line_pos_lam):
            plot_list = [i[1] + const_var.plot_shift for i in tmp_list if i[0] == lamp]
            peak_x_list.extend(plot_list)
            peak_y_list.extend([fiber_count + const_var.plot_shift] * len(plot_list))

        self.i1.plot(x=peak_x_list, y=peak_y_list, pen=None, symbol='o', symbolSize=3, symbolBrush=(0, 255, 0))

# draw marks in spectrum
        for tmp_line in self.line_pos_lam[self.fiberID - 1]:
            if tmp_line[0] == lamp:
                x_int = int(float(tmp_line[1]))
                if 0 <= x_int < len(self.data) - 1:
                    peak_count = max(self.data[x_int], self.data[x_int + 1])
                    self.s2.plot(x=[tmp_line[1]], y=[peak_count], pen=None, symbol='t', symbolBrush=(0, 255, 0))

    def plotPosLam(self):
        self.g1.clear()
        self.g2.clear()

        tmp_line = self.line_pos_lam[self.fiberID - 1]
#    print('fiberID = {} line_pos_lms = {}'.format(self.fiberID, tmp_line))
        if len(tmp_line) > 0:
            peak_array = np.array([i[1:3] for i in tmp_line], dtype='float')
            self.g1.plot(peak_array, pen=None, symbol='+', symbolBrush=(255, 255, 255))

        if len(tmp_line) > 3:
            x_resi_array, self.f_cheby = data_red.wave_calib.fitResidual(peak_array)
            self.g2.plot(x_resi_array, pen=None, symbol='+', symbolBrush=(255, 255, 255))

## functions for data reduction
    def markPeak(self):
        peak_x = data_red.wave_calib.measurePeak(self.data, int(self.mouse_x))

        lamp = self.comboComp.currentText()
        grism_id = const_var.grism_names.index(self.comboGrism.currentText())

        if hasattr(self, 'f_cheby'):
            near_id = 0
            for line_id, tmp_lam in enumerate(linelists[lamp]):
                if -2 < (self.f_cheby(peak_x) - tmp_lam) < 2:
                    near_id = line_id
            self.line_pos_lam[self.fiberID - 1].append([lamp, peak_x, linelists[lamp][near_id]])
        else:
            self.line_pos_lam[self.fiberID - 1].append([lamp, peak_x, linelists[lamp][0]])

        self.writeTable()

    def markPeakAll(self):
        self.readTable()
        grism = self.comboGrism.currentText().lower()

        for tmp_lamp in const_var.comp_names:
            if not os.path.isfile('{}-{}-extract.fits'.format(tmp_lamp, grism)):
                continue

            input_fits = '{}-{}-extract.fits'.format(tmp_lamp, grism)
            data_whole = fits.getdata(input_fits, view=np.ndarray)

            for tmp_line in self.line_pos_lam[self.fiberID - 1]:
                if tmp_line[0] != tmp_lamp:
                    continue

                peak_xy = data_red.wave_calib.markPeakAll(data_whole, round(tmp_line[1]), self.fiberID - 1, fiberbundle_id=self.fiberbundle_id)

                for tmp_peak_x, tmp_fiber in peak_xy:
                    for tmp2_line in self.line_pos_lam[tmp_fiber]:
                        if abs(tmp_peak_x - tmp2_line[1]) < min_peak_sep:
                            break
                    else:
                        self.line_pos_lam[tmp_fiber].append([tmp_line[0], tmp_peak_x, tmp_line[2]])

        self.writeTable()

    def waveCalib(self):
# get grism ID
        grism = self.comboGrism.currentText().lower()
        grism_names_lower = [i.lower() for i in const_var.grism_names]

        if grism in grism_names_lower:
            grism_id = grism_names_lower.index(grism)
        else:
            print('Grism name does not match.')
            return

        input_list = common_func.add_suffix(const_var.frames['object'][grism_id], '-extract') + ['flat-{}-extract.fits'.format(grism)]
#    output_list = common_func.add_suffix(const_var.frames['object'][grism_id], '-wc')
        file_lib = 'lib/line-pos-lam-{}.dat'.format(grism)

        print('Wavelength calibration starts.')

# fork process
        num_cpu = int(const_var.cpuThreadLimit)
        input_list_div = common_func.divide_frame_list(input_list, num_cpu)

        def reduction(in_list):
            output_list = [i.replace('-extract.fits', '-wc.fits') for i in in_list]

            for input_fits, output_fits in zip(in_list, output_list):
                if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                    data_red.wave_calib.wave_calib(input_fits, output_fits, grism, file_lib)

        jobs = []
        for tmp_input in input_list_div:
            job = multiprocessing.Process(target=reduction, args=(tmp_input,))
            jobs.append(job)
            job.start()

        [job.join() for job in jobs]

# make flat frame
#        data_red.wave_calib.makeFlat('flat-{}-wc.fits'.format(grism), 'lib/norm-flat-{}.fits'.format(grism))

        input_list = common_func.add_suffix(const_var.frames['twilight'][grism_id], '-wc')
        if len(input_list) == 0:
            print('Wavelength calibration finished.')
            return

        data_red.wave_calib.make_twilight_flat(input_list, 'lib/norm-flat-{}.fits'.format(grism))

# divide flat
        input_list = common_func.add_suffix(const_var.frames['object'][grism_id], '-wc')

        for tmp_input in input_list:
            input_fits = tmp_input
            output_fits = tmp_input.replace('-wc.fits', '-wc-norm.fits')
            if const_var.frameOverwrite == 'Yes' or not os.path.isfile(output_fits):
                data_red.data_func.arith_frame(input_fits, 'div', 'lib/norm-flat-{}.fits'.format(grism), output_fits)
                data_red.data_func.make_3d_image(output_fits)

        print('Wavelength calibration finished.')

        return

## small functions
    def updateGrismList(self):
        self.comboGrism.clear()
        for tmp_grism in const_var.grism_names:
            for tmp_lamp in const_var.comp_names:
                if os.path.isfile('{}-{}-extract.fits'.format(tmp_lamp, tmp_grism.lower())) \
                  or os.path.isfile('lib/line-pos-lam-{}.dat'.format(tmp_grism.lower())):
                    self.comboGrism.addItem(tmp_grism)
                    break

    def changeGrism(self):
# update the comparison list
        self.comboComp.clear()
        grism = self.comboGrism.currentText().lower()
        for tmp_lamp in const_var.comp_names:
            input_fits = ('{}-{}-extract.fits'.format(tmp_lamp, grism))
            if os.path.isfile(input_fits):
                self.comboComp.addItem(tmp_lamp)

# clear the line list
        self.line_pos_lam = [[] for i in range(self.n_fiber)]
        self.tableLines.setRowCount(0)

# open the comparison frame
        if hasattr(self, 's1') and os.path.isfile(input_fits):
            self.openGraphWindow()

    def changeFiberID(self, diff):
        self.readTable()
        self.fiberID = data_red.data_func.checkFiberID(int(self.boxFiberID.text()) + int(diff))
        self.boxFiberID.setText(str(self.fiberID))

        self.writeTable()
        self.loadWindow()

    def writeTable(self):
        self.line_pos_lam[self.fiberID - 1] = sorted(self.line_pos_lam[self.fiberID - 1], key=sort_column)

        tmp_pos_lam = self.line_pos_lam[int(self.boxFiberID.text()) - 1]
        grism_id = const_var.grism_names.index(self.comboGrism.currentText())

        def findNearLine(lamp, lam):
            lam_float = float(lam)
            near_id = 0
            for line_id, tmp_lam in enumerate(linelists[lamp]):
                if -2 < (lam_float - tmp_lam) < 2:
                    near_id = line_id

            return near_id

        if self.tableLines.rowCount() > 0:
            self.tableLines.setRowCount(0)

        for row_number, tmp_values in enumerate(tmp_pos_lam):
            lamp = str(tmp_values[0])

            self.tableLines.insertRow(row_number)
            item = QTableWidgetItem(lamp)
            item.setFlags(Qt.ItemIsEnabled)
            self.tableLines.setItem(row_number, 0, item)

            item = QTableWidgetItem(str(tmp_values[1]))
            self.tableLines.setItem(row_number, 1, item)

            comboLinelists = QComboBox()
            comboLinelists.setFont(const_var.font)
            for lam in linelists[lamp]:
                comboLinelists.addItem(str(lam))
            comboLinelists.setCurrentIndex(findNearLine(lamp, tmp_values[2]))
            comboLinelists.currentIndexChanged.connect(self.readTable)
            comboLinelists.currentIndexChanged.connect(self.loadWindow)
            self.tableLines.setCellWidget(row_number, 2, comboLinelists)

    def readTable(self):
        tmp_list = []

        for count in range(self.tableLines.rowCount()):
            tmp_list.append([self.tableLines.item(count, 0).text(), float(self.tableLines.item(count, 1).text()), self.tableLines.cellWidget(count, 2).currentText()])

        self.line_pos_lam[self.fiberID - 1] = tmp_list

    def importLines(self):
        file_in = 'lib/line-pos-lam-{}.dat'.format(self.comboGrism.currentText().lower())
        if not os.path.isfile(file_in):
            print('Line data file is not found.')
            return 1

        self.line_pos_lam = []

        with open(file_in, 'r') as fin:
            input_lines = fin.readlines()

        for count_fiber, line in enumerate(input_lines):
            if count_fiber >= self.n_fiber:
                break

            tmp_list = []
            line_split = line.split(' ')

            while len(line_split) >= 3:
                tmp_list.append([line_split[0], float(line_split[1]), line_split[2]])
                del line_split[0:3]

            self.line_pos_lam.append(tmp_list)

        self.writeTable()
        self.loadWindow()

    def exportLines(self):
        file_out = 'lib/line-pos-lam-{}.dat'.format(self.comboGrism.currentText().lower())
        with open(file_out, 'w') as fout:
            for tmp_lines in self.line_pos_lam:
                for line in tmp_lines:
                    fout.write('{0[0]} {0[1]} {0[2]} '.format(line))
                fout.write('\n')

        print('Export {}'.format(file_out))

    def refit_lines(self):
        pass

    def delete_line(self):
        if self.tableLines.currentItem() is not None:
            row = self.tableLines.currentRow()
            del self.line_pos_lam[int(self.boxFiberID.text()) - 1][row]
            self.tableLines.removeRow(self.tableLines.currentRow())

            self.loadWindow()

    def delete_lines(self):
        if self.tableLines.currentItem() is not None:
            row = self.tableLines.currentRow()
            lamp, pos, lam = self.line_pos_lam[int(self.boxFiberID.text()) - 1][row]
            for count_fiber in range(len(self.line_pos_lam)):
                for count_line in range(len(self.line_pos_lam[count_fiber])):
                    tmp_list = self.line_pos_lam[count_fiber][count_line]
                    if tmp_list[0] == lamp and tmp_list[2] == lam:
                        self.line_pos_lam[count_fiber].pop(count_line)
                        break

            del self.line_pos_lam[int(self.boxFiberID.text()) - 1][row]
            self.tableLines.removeRow(self.tableLines.currentRow())

            self.loadWindow()

    def delete_all_lines(self):
        self.line_pos_lam = [[] for i in range(self.n_fiber)]
        self.writeTable()

    def updateGUI(self):
        self.updateGrismList()
