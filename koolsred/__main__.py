#!/usr/bin/env python3

### __main__.py: the main program of triccsred

### import libraries
import argparse

# KOOLS-IFU reduction modules
import koolsred

def main(args):
    koolsred.main_window.show(args)

    return

if __name__ == '__main__':
# argparse
    parser = argparse.ArgumentParser(description='KOOLS=IFU data reduction GUI')
#    parser.add_argument('-t', '--test', action='store_true', help='Test.')

    main(parser.parse_args())

