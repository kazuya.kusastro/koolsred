Add the PYTHONPATH to use koolsred module

Example:
# at ~/.bash_aliases
export PYTHONPATH="(koolsred path):$PYTHONPATH"

Usage:
python3 -m koolsred

